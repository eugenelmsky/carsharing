﻿using System.Data;

namespace CarSharing.DAL.Repositories
{
    public abstract class BaseRepository
    {
        protected IDbTransaction Transaction { get; }
        protected IDbConnection Connection => Transaction.Connection;

        protected BaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}
