﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class DriverRepository:DapperRepository<Driver>, IDriverRepository
    {
        private readonly IDbTransaction _transaction;

        public DriverRepository(IDbConnection connection, ISqlGenerator<Driver> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<Driver>> GetAll()
        {
            return await FindAllAsync(transaction: _transaction);
        }

        public async Task<Driver> GetById(int id)
        {
            return await FindByIdAsync(id, _transaction);
        }

        public async Task<IEnumerable<Driver>> Find(Expression<Func<Driver, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(Driver item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(Driver item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id, _transaction);
        }

        public async Task<Driver> GetByIdWithRatingsAndPreference(int id)
        {
            return await FindAsync<DriverRating, DriverPreference>(p => p.Id==id, s => s.DriverRating, r => r.DriverPreference, transaction: _transaction);
        }

        public async Task<IEnumerable<Driver>> GetAllWithRatingsAndPreference()
        {
            return await FindAllAsync<DriverRating,DriverPreference>(null, s => s.DriverRating, r=>r.DriverPreference, transaction:_transaction);
        }

        public async Task<Driver> GetByEmail(string email)
        {
            return await FindAsync(s => s.Email == email, _transaction);
        }

        public async Task<Driver> GetByUsername(string username)
        {
            return await FindAsync(x=>x.Username==username,_transaction);
        }

        public async Task<Driver> GetWithPreference(string username)
        {
            return await FindAsync<DriverPreference>(x => x.Username == username, q => q.DriverPreference, transaction: _transaction);
        }
    }
}