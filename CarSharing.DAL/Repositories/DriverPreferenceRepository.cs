﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class DriverPreferenceRepository : DapperRepository<DriverPreference>, IDriverPreferenceRepository
    {
        private readonly IDbTransaction _transaction;

        public DriverPreferenceRepository(IDbConnection connection, ISqlGenerator<DriverPreference> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }


        public async Task<IEnumerable<DriverPreference>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<DriverPreference> GetById(int id)
        {
            return await FindByIdAsync(id, transaction: _transaction);
        }

        public async Task<IEnumerable<DriverPreference>> Find(Expression<Func<DriverPreference, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(DriverPreference item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(DriverPreference item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id, _transaction);
        }
    }
}