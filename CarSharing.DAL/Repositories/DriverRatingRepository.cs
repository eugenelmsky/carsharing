﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class DriverRatingRepository:DapperRepository<DriverRating>,IDriverRatingRepository
    {
        private readonly IDbTransaction _transaction;

        public DriverRatingRepository(IDbConnection connection, ISqlGenerator<DriverRating> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<DriverRating>> GetByDriverIdWithPassenger(int driverId)
        {
            return await FindAllAsync<Passenger>(x => x.DriverId==driverId, y=>y.Passenger, _transaction);
        }

        public async Task<IEnumerable<DriverRating>> GetByDriverId(int driverId)
        {
            return await Find(s => s.DriverId == driverId);
        }

        public async Task<IEnumerable<DriverRating>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<DriverRating> GetById(int id)
        {
            return await FindAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<DriverRating>> Find(Expression<Func<DriverRating, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(DriverRating item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(DriverRating item)
        {
            await UpdateAsync(item);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(dest => dest.Id == id);
        }
    }


   
}