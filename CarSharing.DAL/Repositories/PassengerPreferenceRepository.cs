﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class PassengerPreferenceRepository : DapperRepository<PassengerPreference>, IPassengerPreferenceRepository
    {
        private readonly IDbTransaction _transaction;

        public PassengerPreferenceRepository(IDbConnection connection, ISqlGenerator<PassengerPreference> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<PassengerPreference>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<PassengerPreference> GetById(int id)
        {
            return await FindByIdAsync(id, transaction: _transaction); ;
        }

        public async Task<IEnumerable<PassengerPreference>> Find(Expression<Func<PassengerPreference, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(PassengerPreference item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(PassengerPreference item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id, _transaction);
        }
    }
}