﻿using System;
using System.Data;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IPassengerRepository _passengerRepository;
        private IPassengerPreferenceRepository _passengerPreferenceRepository;
        private IDriverRepository _driverRepository;
        private IDriverPreferenceRepository _driverPreferenceRepository;
        private IRideRepository _rideRepository;
        private IRequestRepository _requestRepository;
        private IDriverRatingRepository _driverRatingRepository;
        private IPassengerRatingRepository _passengerRatingRepository;
        private readonly SqlGeneratorConfig _sqlGeneratorConfig = new SqlGeneratorConfig { SqlConnector = ESqlConnector.MSSQL };


        private bool _disposed;

        public UnitOfWork(IDbConnection dbConnection)
        {
            _connection = dbConnection;
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IPassengerRepository PassengerRepository
        {
            get
            {
                if(_passengerRepository==null)
                {
                    _passengerRepository = new PassengerRepository(_connection, new SqlGenerator<Passenger>(_sqlGeneratorConfig), _transaction);
                }

                return _passengerRepository;
            }
        }

        public IPassengerPreferenceRepository PassengerPreferenceRepository
        {
            get
            {
                if (_passengerPreferenceRepository == null)
                {
                    _passengerPreferenceRepository = new PassengerPreferenceRepository(_connection, new SqlGenerator<PassengerPreference>(_sqlGeneratorConfig), _transaction);
                }

                return _passengerPreferenceRepository;
            }
        }

        public IDriverRepository DriverRepository
        {
            get
            {
                if (_driverRepository == null)
                {
                    _driverRepository = new DriverRepository(_connection, new SqlGenerator<Driver>(_sqlGeneratorConfig), _transaction);
                }

                return _driverRepository;
            }
        }

        public IDriverPreferenceRepository DriverPreferenceRepository
        {
            get
            {
                if (_driverPreferenceRepository == null)
                {
                    _driverPreferenceRepository = new DriverPreferenceRepository(_connection, new SqlGenerator<DriverPreference>(_sqlGeneratorConfig), _transaction);
                }

                return _driverPreferenceRepository;
            }
        }

        public IRideRepository RideRepository
        {
            get
            {
                if (_rideRepository == null)
                {
                    _rideRepository = new RideRepository(_connection, new SqlGenerator<Ride>(_sqlGeneratorConfig), _transaction);
                }

                return _rideRepository;
            }
        }

        public IRequestRepository RequestRepository
        {
            get
            {
                if (_requestRepository == null)
                {
                    _requestRepository = new RequestRepository(_connection, new SqlGenerator<Request>(_sqlGeneratorConfig), _transaction);
                }

                return _requestRepository;
            }
        }

        public IDriverRatingRepository DriverRatingRepository
        {
            get
            {
                if (_driverRatingRepository == null)
                {
                    _driverRatingRepository = new DriverRatingRepository(_connection, new SqlGenerator<DriverRating>(_sqlGeneratorConfig), _transaction);
                }

                return _driverRatingRepository;
            }
        }

        public IPassengerRatingRepository PassengerRatingRepository
        {
             get
            {
                if (_passengerRatingRepository == null)
                {
                    _passengerRatingRepository = new PassengerRatingRepository(_connection, new SqlGenerator<PassengerRating>(_sqlGeneratorConfig), _transaction);
                }

                return _passengerRatingRepository;
            }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                ResetRepositories();
            }
        }

        private void ResetRepositories()
        {
            _passengerRepository = null;
            _passengerPreferenceRepository = null;
            _driverRepository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }

                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }

                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}