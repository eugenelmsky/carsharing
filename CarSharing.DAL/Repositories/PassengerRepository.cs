﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class PassengerRepository : DapperRepository<Passenger>, IPassengerRepository
    {
        private readonly IDbTransaction _transaction;

        public PassengerRepository(IDbConnection connection, ISqlGenerator<Passenger> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<Passenger>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<Passenger> GetById(int id)
        {
            return await FindByIdAsync(id, _transaction);
        }

        public async Task<IEnumerable<Passenger>> Find(Expression<Func<Passenger, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(Passenger item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(Passenger item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<Passenger>> GetAllWithRatingsAndPreference()
        {
            return await FindAllAsync<PassengerRating, PassengerPreference>(null, s => s.PassengerRating, r => r.PassengerPreference, transaction: _transaction);
        }

        public async Task<Passenger> GetByEmail(string email)
        {
            return await FindAsync(s => s.Email == email, _transaction);
        }

        public async Task<Passenger> GetByUsername(string username)
        {
            return await FindAsync(s => s.Username == username,_transaction);
        }

        public async Task<Passenger> GetWithPreference(string username)
        {
            return await FindAsync<PassengerPreference>(x => x.Username == username, q => q.PassengerPreference, _transaction);
        }

        public async Task<Passenger> GetWithRatingById(int id)
        {
            return await FindAsync(s => s.Id == id, _transaction);
        }
    }
}