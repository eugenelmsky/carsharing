﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class RequestRepository: DapperRepository<Request>, IRequestRepository
    {
        private readonly IDbTransaction _transaction;

        public RequestRepository(IDbConnection connection, ISqlGenerator<Request> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<Request>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<Request> GetById(int id)
        {
            return await FindByIdAsync(id, transaction: _transaction);
        }

        public async Task<IEnumerable<Request>> Find(Expression<Func<Request, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(Request item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(Request item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id, _transaction);
        }

        public async Task<IEnumerable<Request>> GetByRideIdWithPassenger(int id)
        {
            return await FindAllAsync<Passenger>(x => x.RideId == id, q => q.Passenger, transaction: _transaction);
        }   

        public async Task<IEnumerable<Request>> GetAllWithRideByPassengerId(int passengerId)
        {
            return await FindAllAsync<Ride>(x => x.PassengerId == passengerId, q => q.Ride, transaction: _transaction);
        }

        public async Task<IEnumerable<Request>> GetByRideId(int rideId)
        {
            return await FindAllAsync(s => s.RideId == rideId, transaction: _transaction);
        }
    }
}