﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using Dapper;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class RideRepository : DapperRepository<Ride>, IRideRepository
    {
        private readonly IDbTransaction _transaction;

        public RideRepository(IDbConnection connection, ISqlGenerator<Ride> sqlGenerator, IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<Ride>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<Ride> GetById(int id)
        {
            return await FindByIdAsync(id, transaction: _transaction);
        }

        public async Task<IEnumerable<Ride>> Find(Expression<Func<Ride, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(Ride item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(Ride item)
        {
            await UpdateAsync(item, _transaction);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(s => s.Id == id, _transaction);
        }

        public async Task<IEnumerable<Ride>> GetByDriverId(int driverId)
        {
            return await FindAllAsync(s => s.DriverId == driverId, _transaction);
        }

        public async Task<IEnumerable<Ride>> GetAllWithDrivers()
        {
            //TODO:check
            return await FindAllAsync<Driver>(null, q => q.Driver, _transaction);
        }

        public async Task<IEnumerable<Ride>> Search(string sourceCity, string destinationCity, DateTime? startDate)
        {
            var builder = new SqlBuilder();
            var selector =
                builder.AddTemplate(
                    "SELECT * from Rides AS A INNER JOIN Drivers AS B ON A.DriverId = B.Id /**where**/");


            if (!string.IsNullOrEmpty(sourceCity))
                builder.Where("SourceCity = @SourceCity");

            if (!string.IsNullOrEmpty(destinationCity))
                builder.Where("DestinationCity = @DestinationCity");

            if (startDate != null)
            {
                builder.OrWhere($"(CONVERT(VARCHAR(25), A.StartTime, 25) LIKE '{startDate.Value:yyyy-MM-dd}%')");
            }

            ;

            var sql = selector.RawSql;


            var rides = await Connection.QueryAsync<Ride, Driver, Ride>(
                sql,
                (ride, driver) =>
                {
                    ride.Driver = driver;
                    return ride;
                }, param: new {SourceCity = sourceCity, destinationCity},
                splitOn: "Id", transaction: _transaction);
            return rides.Distinct().ToList();
        }
    }
}