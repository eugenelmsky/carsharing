﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace CarSharing.DAL.Repositories
{
    public class PassengerRatingRepository : DapperRepository<PassengerRating>, IPassengerRatingRepository
    {
        private readonly IDbTransaction _transaction;

        public PassengerRatingRepository(IDbConnection connection, ISqlGenerator<PassengerRating> sqlGenerator,
            IDbTransaction transaction)
            : base(connection, sqlGenerator)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<PassengerRating>> GetAll()
        {
            return await FindAllAsync(_transaction);
        }

        public async Task<PassengerRating> GetById(int id)
        {
            return await FindAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<PassengerRating>> Find(Expression<Func<PassengerRating, bool>> predicate)
        {
            return await FindAllAsync(predicate, _transaction);
        }

        public async Task Create(PassengerRating item)
        {
            await InsertAsync(item, _transaction);
        }

        public async Task Update(PassengerRating item)
        {
            await UpdateAsync(item);
        }

        public async Task DeleteById(int id)
        {
            await DeleteAsync(dest => dest.Id == id);
        }

        public async Task<IEnumerable<PassengerRating>> GetByPassengerIdWithDriver(int passengerId)
        {
            return await FindAllAsync<Driver>(x => x.PassengerId == passengerId, y => y.Driver, _transaction);
        }

        public async Task<IEnumerable<PassengerRating>> GetByPassengerId(int passengerId)
        {
            return await FindAllAsync(x => x.PassengerId == passengerId, _transaction);
        }
    }
}