﻿using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IPassengerPreferenceRepository:IRepository<PassengerPreference>
    {
    }
}