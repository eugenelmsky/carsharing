﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IPassengerRepository : IRepository<Passenger>
    {
        Task<IEnumerable<Passenger>> GetAllWithRatingsAndPreference();
        Task<Passenger> GetByEmail(string email);
        Task<Passenger> GetByUsername(string username);
        Task<Passenger> GetWithPreference(string username);
    }
}