﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IDriverRepository:IRepository<Driver>
    {
        Task<Driver> GetByIdWithRatingsAndPreference(int id);
        Task<IEnumerable<Driver>> GetAllWithRatingsAndPreference();
        Task<Driver> GetByEmail(string email);
        Task<Driver> GetByUsername(string username);
        Task<Driver> GetWithPreference(string username);
    }
}