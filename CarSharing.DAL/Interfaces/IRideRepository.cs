﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IRideRepository:IRepository<Ride>
    {
        Task<IEnumerable<Ride>> GetByDriverId(int id);
        Task<IEnumerable<Ride>> GetAllWithDrivers();
        Task<IEnumerable<Ride>> Search(string sourceCity, string destinationCity, DateTime? startDate);
    }
}