﻿using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IDriverPreferenceRepository : IRepository<DriverPreference>
    {
    }
}