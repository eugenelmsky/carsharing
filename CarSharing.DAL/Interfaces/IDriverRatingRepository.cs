﻿using System.Collections.Generic;
using CarSharing.DAL.Entities;
using System.Threading.Tasks;

namespace CarSharing.DAL.Interfaces
{
    public interface IDriverRatingRepository:IRepository<DriverRating>
    {
        Task<IEnumerable<DriverRating>> GetByDriverIdWithPassenger(int driverId);

        Task<IEnumerable<DriverRating>> GetByDriverId(int driverId);

    }
}