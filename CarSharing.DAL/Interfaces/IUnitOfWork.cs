﻿using System;

namespace CarSharing.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IPassengerRepository PassengerRepository { get; }
        IPassengerPreferenceRepository PassengerPreferenceRepository { get; }
        IDriverRepository DriverRepository { get; }
        IDriverPreferenceRepository DriverPreferenceRepository { get; }
        IRideRepository RideRepository { get; }
        IRequestRepository RequestRepository { get; }
        IDriverRatingRepository DriverRatingRepository { get; }
        IPassengerRatingRepository PassengerRatingRepository { get; }
        void Commit();
    }
}
