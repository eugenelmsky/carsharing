﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IRequestRepository:IRepository<Request>
    {
        Task<IEnumerable<Request>> GetByRideIdWithPassenger(int id);
        Task<IEnumerable<Request>> GetAllWithRideByPassengerId(int passengerId);
        Task<IEnumerable<Request>> GetByRideId(int rideId);
    }
}