﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.DAL.Entities;

namespace CarSharing.DAL.Interfaces
{
    public interface IPassengerRatingRepository:IRepository<PassengerRating>
    {
        Task<IEnumerable<PassengerRating>> GetByPassengerIdWithDriver(int passengerId);
        Task<IEnumerable<PassengerRating>> GetByPassengerId(int passengerId);
    }
}