﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarSharing.DAL.Entities
{
    [Table("PassengerPreferences")]
    public class PassengerPreference
    {
        [Key]
        public int Id { get; set; }
        public bool SmokingAllowed { get; set; }
        public bool PetAllowed { get; set; }
        public bool MusicAllowed { get; set; }
        public bool ChitChatAllowed { get; set; }
    }
}