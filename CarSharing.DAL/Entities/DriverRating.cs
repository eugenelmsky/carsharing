﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace CarSharing.DAL.Entities
{
    [Table("DriversRatings")]
    public class DriverRating
    {
        [Key,Identity]

        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int DriverId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }
        [InnerJoin("Drivers","DriverId","Id")]
        public Driver Driver { get; set; }
        [InnerJoin("Passengers","PassengerId","Id")]
        public Passenger Passenger { get; set; }
    }
}