﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace CarSharing.DAL.Entities
{
    [Table("Passengers")]
    public class Passenger
    {
        [Key, Identity]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string ProfileImage { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime? PasswordResetTokenCreatedOn { get; set; }
        [InnerJoin("PassengerPreferences", "Id", "Id")]
        public PassengerPreference PassengerPreference { get; set; }
        public DriverPreference DriverPreference { get; set; }
        [LeftJoin("PassengersRatings", "Id", "PassengerId")]
        public IEnumerable<PassengerRating> PassengerRating { get; set; }
    }
}