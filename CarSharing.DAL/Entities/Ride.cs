﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace CarSharing.DAL.Entities
{
    [Table("Rides")]
    public class Ride
    {
        [Key, Identity]
        public int Id { get; set; }
        public int DriverId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string SourceCity { get; set; }
        public string DestinationCity { get; set; }
        public string ContributionPerHead { get; set; } 
        public string LuggageSize { get; set; }
        public string Seats { get; set; }
        public string Description { get; set; }
        [InnerJoin("Drivers", "DriverId", "Id")]
        public Driver Driver { get; set; }
    }
}