﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace CarSharing.DAL.Entities
{
    [Table("DriverPreferences")]
    public class DriverPreference
    {
        [Key]
        public int Id { get; set; }
        public bool SmokingAllowed { get; set; }
        public bool PetAllowed { get; set; }
        public bool MusicAllowed { get; set; }
        public bool ChitChatAllowed { get; set; }
    }
}