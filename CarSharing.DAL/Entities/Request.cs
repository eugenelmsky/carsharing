﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace CarSharing.DAL.Entities
{
    [Table("Requests")]
    public class Request
    {
        [Key, Identity]
        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int RideId { get; set; }
        public string DestinationCity { get; set; }
        public bool? RequestStatus { get; set; }
        public string Message { get; set; }

        [InnerJoin("Passengers", "PassengerId", "Id")]
        public Passenger Passenger { get; set; }

        [InnerJoin("Rides", "RideId", "Id")]
        public Ride Ride { get; set; }
    }
}