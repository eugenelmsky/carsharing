﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.Web.Filters;
using CarSharing.Web.Models;
using CarSharing.Web.Services;

namespace CarSharing.Web.Controllers
{
    [Culture]
    public class GuestController : Controller
    {
        private readonly IFormsAuthenticationService _authenticationService;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper;
        private readonly IPassengerService _passengerService;
        private readonly IDriverService _driverService;


        public GuestController(IPassengerService passengerService, IDriverService driverService, IFormsAuthenticationService authenticationService,
            IEmailService emailService, IMapper mapper)
        {
            _authenticationService = authenticationService;
            _emailService = emailService;
            _mapper = mapper;
            _passengerService = passengerService;
            _driverService = driverService;
        }

        [HttpGet]
        public ActionResult LoginPassenger()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginPassenger(UserLoginViewModel userAccount)
        {
            if (ModelState.IsValid)
            {
                bool isAuthenticated = await _passengerService.Login(userAccount.Username, userAccount.Password);
                if (isAuthenticated)
                {
                    _authenticationService.SignIn(userAccount.Username, false, Role.Passenger);
                    return RedirectToAction("Index", "Ride");
                }

                ModelState.AddModelError("", Resources.Resource.InvalidUsernameOrPassword);
            }

            return View(userAccount);
        }

        [HttpGet]
        public ActionResult LoginDriver()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginDriver(UserLoginViewModel userAccount)
        {
            if (ModelState.IsValid)
            {
                bool isAuthenticated = await _driverService.Login(userAccount.Username, userAccount.Password);
                if (isAuthenticated)
                {
                    _authenticationService.SignIn(userAccount.Username, true, Role.Driver);
                    return RedirectToAction("Index", "Ride");
                }

                ModelState.AddModelError("", Resources.Resource.InvalidUsernameOrPassword);
            }

            return View(userAccount);
        }

        [HttpGet]
        public ActionResult RegisterPassenger()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterPassenger(PassengerRegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var passengerRegistrationDto = _mapper.Map<PassengerRegistrationDTO>(model);
                await _passengerService.CreatePassenger(passengerRegistrationDto);

                _authenticationService.SignIn(model.Username, true, Role.Passenger);

                return RedirectToAction("Profile", "Passenger", new {id = model.Username});
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult RegisterDriver()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterDriver(DriverRegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var driverRegistrationDto = _mapper.Map<DriverRegistrationDTO>(model);
                await _driverService.CreateDriver(driverRegistrationDto);

                _authenticationService.SignIn(model.Username, true, Role.Driver);

                return RedirectToAction("Profile", "Driver", new {id = model.Username});
            }

            return View(model);
        }



        public ActionResult Logoff()
        {
            _authenticationService.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult ForgotPassword(string accountType)
        {
            ViewBag.AccountType = accountType;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.AccountType == "Passenger")
                {
                    var passengerDto = await _passengerService.GetByEmaill(model.Email);
                    if (passengerDto == null)
                    {
                        ModelState.AddModelError("", Resources.Resource.EmailNotExistErrorMessage);
                        return View(model);
                    }

                    var passwordResetToken = await _passengerService.CreatePasswordResetToken(model.Email);

                    string verifyUrl =
                        $"/Guest/ResetPassword?code={passwordResetToken}&accounttype={model.AccountType}&email={model.Email}";
                    var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

                    await _emailService.SendPasswordRecoveryCodeAsync(passengerDto.Email, passwordResetToken, link);
                    ViewBag.Message = Resources.Resource.ResetPasswordMessage;
                }

                if (model.AccountType == "Driver")
                {
                    var driverDto = await _driverService.GetByEmaill(model.Email);
                    if (driverDto == null)
                    {
                        ModelState.AddModelError("", Resources.Resource.EmailNotExistErrorMessage);
                        return View(model);
                    }

                    var passwordResetToken = await _driverService.CreatePasswordResetToken(model.Email);

                    string verifyUrl =
                        $"/Guest/ResetPassword?code={passwordResetToken}&accounttype={model.AccountType}&email={model.Email}";
                    var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

                    await _emailService.SendPasswordRecoveryCodeAsync(driverDto.Email, passwordResetToken, link);
                    ViewBag.Message = Resources.Resource.ResetPasswordMessage;
                }
            }

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ResetPassword(string code, string accountType, string email)
        {
            bool isVerified = false;
            if (accountType=="Passenger")
            {
                isVerified = await _passengerService.VerifyPasswordResetToken(code, email);
            }

            if (accountType == "Driver")
            {
                isVerified = await _driverService.VerifyPasswordResetToken(code, email);
            }

            if (isVerified==false)
            {
                return HttpNotFound();
            }
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                AccountType = accountType,
                Code = code,
                Email = email
            };

            return View(resetPasswordViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.AccountType=="Passenger")
                    {
                        await _passengerService.ResetPassword(model.Email, model.Password);
                        return RedirectToAction("LoginPassenger", "Guest");
                    }

                    if (model.AccountType=="Driver")
                    {
                        await _driverService.ResetPassword(model.Email, model.Password);
                        return RedirectToAction("LoginDriver", "Guest");
                    }
                    
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View();
        }

        public ActionResult ChangeLanguage(string lang)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() {"ru", "en"};
            if (!cultures.Contains(lang))
            {
                lang = "ru";
            }

            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang; // если куки уже установлено, то обновляем значение
            else
            {
                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }

            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }
    }
}