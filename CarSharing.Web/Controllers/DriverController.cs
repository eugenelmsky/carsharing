﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.Web.Filters;
using CarSharing.Web.Models;
using CarSharing.Web.Services;

namespace CarSharing.Web.Controllers
{
    [Culture]
    public class DriverController : Controller
    {
        private readonly IDriverService _driverService;
        private readonly IMapper _mapper;
        private readonly IFileService _fileService;
        private readonly IRatingsService _ratingsService;
        private readonly IPassengerService _passengerService;

        public DriverController(IDriverService driverService, IMapper mapper, IFileService fileService, IRatingsService ratingsService, IPassengerService passengerService)
        {
            _driverService = driverService;
            _mapper = mapper;
            _fileService = fileService;
            _ratingsService = ratingsService;
            _passengerService = passengerService;
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public new async Task<ActionResult> Profile(string id)
        {
            var driverDto = await _driverService.GetWithPreferencesByUsername(id);
            if (driverDto == null)
            {
                return HttpNotFound();
            }

            var driverProfileViewModel = _mapper.Map<DriverProfileViewModel>(driverDto);

            return View(driverProfileViewModel);
        }

        [Authorize(Roles = "Driver")]
        [HttpGet]
        public async Task<ActionResult> Edit()
        {

                var driverDto = await _driverService.GetWithPreferencesByUsername(User.Identity.Name);
                if (driverDto == null)
                {
                    return HttpNotFound();
                }

                var driverEditViewModel = _mapper.Map<DriverEditViewModel>(driverDto);

                return View(driverEditViewModel);
        }

        [Authorize(Roles = "Driver")]
        [HttpPost]
        public async Task<ActionResult> Edit(DriverEditViewModel driver)
        {
            if (ModelState.IsValid)
            {
                var driverDto = _mapper.Map<DriverEditDTO>(driver);
                driverDto.Username = User.Identity.Name;

                await _driverService.Update(driverDto);

                return RedirectToAction("Profile", new { id = User.Identity.Name });
            }

            return View();
        }

        [Authorize(Roles = "Driver")]
        public ActionResult UpdateProfileImage()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<ActionResult> UpdateProfileImage(HttpPostedFileBase file)
        {
            var path = _fileService.Upload(file, User.Identity.Name);

            await _driverService.UpdateProfileImage(path, User.Identity.Name);
            return RedirectToAction("Profile", "Driver", new { id = User.Identity.Name });
        }

        [Authorize(Roles = "Driver")]
        public async Task DeleteProfileImage()
        {
            await _driverService.DeleteProfileImage(User.Identity.Name);
        }

        [HttpGet]
        public async Task<ActionResult> Rate(string id)
        {
            var passenger = await _passengerService.GetByUserName(id);
            if (passenger==null)
            {
                return HttpNotFound();
            }

            return View(new RatingPassengerCreateViewModel(){PassengerId = passenger.Id});
        }

        [HttpPost]
        public async Task<ActionResult> Rate(RatingPassengerCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var passenger = await _passengerService.GetById(model.PassengerId);
                if (passenger == null)
                {
                    return HttpNotFound();
                }

                var driver = await _driverService.GetByUserName(User.Identity.Name);
                if (driver == null)
                {
                    HttpNotFound();
                }

                //Get the table of the passenger's rating
                var rating = await _ratingsService.GetByPassengerId(model.PassengerId);

                //The Driver rated this passenger?
                var isRatingExist = rating.Where(s => s.DriverId == driver.Id);

                //If isRatingExist are no records create PassengerRatingDto
                if (!isRatingExist.Any())
                {
                    var ratingDto = _mapper.Map<PassengerRatingDTO>(model);
                    ratingDto.CreatedOn = DateTime.Now;
                    ratingDto.DriverId = driver.Id;
                    await _ratingsService.RateThePassenger(ratingDto);
                    return RedirectToAction("Profile", "Passenger", new {id = passenger.Username});
                }

                ModelState.AddModelError("", Resources.Resource.RatingExistErrorMessage);
            }

            return View();
        }
    }
}