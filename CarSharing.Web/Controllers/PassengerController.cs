﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.Web.Filters;
using CarSharing.Web.Models;
using CarSharing.Web.Services;

namespace CarSharing.Web.Controllers
{
    [Culture]
    public class PassengerController : Controller
    {
        private readonly IPassengerService _passengerService;
        private readonly IMapper _mapper;
        private readonly IFileService _fileService;
        private readonly IDriverService _driverService;
        private readonly IRatingsService _ratingsService;

        public PassengerController(IPassengerService passengerService, IMapper mapper, IFileService fileService, IDriverService driverService, IRatingsService ratingsService)
        {
            _passengerService = passengerService;
            _mapper = mapper;
            _fileService = fileService;
            _driverService = driverService;
            _ratingsService = ratingsService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public new async Task<ActionResult> Profile(string id)
        {
            var passengerDto = await _passengerService.GetWithPreferencesByUsername(id);
            if (passengerDto == null)
            {
                return HttpNotFound();
            }

            var passengerProfileViewModel = _mapper.Map<PassengerProfileViewModel>(passengerDto);
            return View(passengerProfileViewModel);
        }

        [HttpGet]
        public async Task<ActionResult> Edit()
        {
            var passengerDto = await _passengerService.GetWithPreferencesByUsername(User.Identity.Name);
            if (passengerDto == null)
            {
                return HttpNotFound();
            }

            var passengerEditViewModel = _mapper.Map<PassengerEditViewModel>(passengerDto);
            return View(passengerEditViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(PassengerEditViewModel passenger)
        {
            if (ModelState.IsValid)
            {
                var passengerDto = _mapper.Map<PassengerEditDTO>(passenger);
                passengerDto.Username = User.Identity.Name;

                await _passengerService.Update(passengerDto);

                return RedirectToAction("Profile", new { id = User.Identity.Name });
            }

            return View();
        }

        public ActionResult UpdateProfileImage()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> UpdateProfileImage(HttpPostedFileBase file)
        {
            var path = _fileService.Upload(file, User.Identity.Name);

            await _passengerService.UpdateProfileImage(path, User.Identity.Name);
            return RedirectToAction("Profile", "Passenger", new { id = User.Identity.Name });
        }

        public async Task DeleteProfileImage()
        {
            await _passengerService.DeleteProfileImage(User.Identity.Name);
        }

        [HttpGet]
        public async Task<ActionResult> Rate(string id)
        {
            var driver = await _driverService.GetByUserName(id);
            if (driver == null)
            {
                return HttpNotFound();
            }

            return View(new RatingDriverCreateViewModel() {DriverId = driver.Id});
        }

        [HttpPost]
        public async Task<ActionResult> Rate(RatingDriverCreateViewModel model)
        {
            var passenger = await _passengerService.GetByUserName(User.Identity.Name);

            if (passenger == null)
            {
                HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var driver = _driverService.GetById(model.DriverId);
                if (driver == null)
                {
                    HttpNotFound();
                }

                //Get the table of the driver's rating
                var rating = await _ratingsService.GetByDriverId(model.DriverId);

                //The Passenger rated this driver?
                var isRatingExist = rating.Where(s => s.PassengerId == passenger.Id);

                //If isRatingExist are no records create DriverRatingDTO
                if (!isRatingExist.Any())
                {
                    var ratingDto = _mapper.Map<DriverRatingDTO>(model);
                    ratingDto.CreatedOn = DateTime.Now;
                    ratingDto.PassengerId = passenger.Id;

                    await _ratingsService.RateTheDriver(ratingDto);
                    return RedirectToAction("Index", "Request");
                }

                ModelState.AddModelError("", @Resources.Resource.RatingExistErrorMessage);
            }

            return View();
        }
    }
}