﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.Web.Filters;
using CarSharing.Web.Models;

namespace CarSharing.Web.Controllers
{
    [Culture]
    public class RequestController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IRequestService _requestService;
        private readonly IPassengerService _passengerService;
        private readonly IRideService _rideService;

        public RequestController(IMapper mapper, IRequestService requestService, IRideService rideService, IPassengerService passengerService)
        {
            _mapper = mapper;
            _requestService = requestService;
            _rideService = rideService;
            _passengerService = passengerService;
        }
        
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var requests = await _requestService.GetAllWithRideByPassengerUsername(User.Identity.Name);
            var viewModel = _mapper.Map<IEnumerable<RequestWithRideViewModel>>(requests);
            return View(viewModel);
        }

        [Authorize(Roles = "Passenger")]
        [HttpGet]
        public async Task<ActionResult> Create(int id)
        {
            var ride = await _rideService.GetById(id);

            if (ride==null)
            {
                return HttpNotFound();
            }
           
            var model = new RequestCreateViewModel {RideId = id};
            return View(model);
        }

        [Authorize(Roles = "Passenger")]
        [HttpPost]
        public async Task<ActionResult> Create(RequestCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var passenger = await _passengerService.GetByUserName(User.Identity.Name);

                if (passenger != null)
                {
                    //Get all the requests for a current trip
                    var requestsForCurrentRide = await _requestService.GetByRideIdWithPassenger(model.RideId);

                    //Check a record with a request from this passenger
                    var isRequestExistByCurrentUser =
                        requestsForCurrentRide.FirstOrDefault(s => s.PassengerId == passenger.Id);

                    //If null create request, else show RequestExistErrorMessage
                    if (isRequestExistByCurrentUser == null)
                    {
                        var requestDto = _mapper.Map<RequestDTO>(model);
                        requestDto.PassengerId = passenger.Id;

                        await _requestService.Create(requestDto);

                        return RedirectToAction("Index");
                    }

                    ModelState.AddModelError("", Resources.Resource.RequestsExistErrorMessage);
                }
            }

            return View();
        }

      [HttpGet]
        public async Task<ActionResult> ChangeRequestStatus(int requestId, bool requestStatus)
        {
            // Get a request by Id
            var request = await _requestService.GetById(requestId);
            if (request == null)
            {
                return HttpNotFound();
            }

            // Get all the rides for this driver
            var allRidesForCurrentDriver = await _rideService.GetByDriverUsername(User.Identity.Name);

            // The request is made to the current driver rides?
            var currentRide = allRidesForCurrentDriver.FirstOrDefault(s => s.Id == request.RideId);
            if (currentRide==null)
            {
                return HttpNotFound();
            }

            // Get all requests for this ride
            var allRequests = await _requestService.GetByRideIdWithPassenger(currentRide.Id);

            // Any available seats?
            var seatsRequested = allRequests.Where(s => s.RequestStatus == true).Count();


            //if requestStatus = true, check the availability
            if (requestStatus)
            {
                if (seatsRequested < currentRide.Seats)
                {
                    request.RequestStatus = requestStatus;
                    await _requestService.Update(request);
                }
            }
            //if requestStatus = false, save to the database
            else
            {
                request.RequestStatus = requestStatus;
                await _requestService.Update(request);
            }
               

         

          

            return RedirectToAction("Rides", "Ride");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var passenger = await _passengerService.GetByUserName(User.Identity.Name);
            if (passenger == null)
            {
                return HttpNotFound();
            }

            var request = await _requestService.GetById(id);
            if (request == null)
            {
                return HttpNotFound();
            }

            if (request.PassengerId != passenger.Id)
            {
                return new HttpUnauthorizedResult();
            }

            var requestViewModel = _mapper.Map<RequestViewModel>(request);
            return View(requestViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(RequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _requestService.DeleteById(model.Id);
            }

            return RedirectToAction("Index", "Request");
        }
    }
}