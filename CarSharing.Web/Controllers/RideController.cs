﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.Web.Filters;
using CarSharing.Web.Models;

namespace CarSharing.Web.Controllers
{
    [Authorize]
    [Culture]
    public class RideController : Controller
    {
        private readonly IRideService _rideService;
        private readonly IDriverService _driverService;
        private readonly IMapper _mapper;

        public RideController(IRideService rideService, IDriverService driverService, IMapper mapper)
        {
            _rideService = rideService;
            _driverService = driverService;
            _mapper = mapper;
        }
        
        [HttpGet]
        public async Task<ActionResult> Index(string sourceCity, string destinationCity, DateTime? startDate)
        {
            var ridesFromDb = await _rideService.Search(new RideSearchDTO
            {
                SourceCity = sourceCity,
                DestinationCity = destinationCity,
                StartDate = startDate
            });
        
            var rides = ridesFromDb.Where(s => s.StartTime > DateTime.Now).ToList();
            var data = _mapper.Map<IEnumerable<RideViewModel>>(rides);

            var rideViewModels = data.OrderBy(s => s.StartTime.Month).ThenBy(s => s.StartTime.Date);

            return View(rideViewModels);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Driver")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RideCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var driver = await _driverService.GetByUserName(User.Identity.Name);
                if (driver==null)
                {
                    return HttpNotFound();
                }
                var rideDto = _mapper.Map<RideDTO>(model);
                rideDto.DriverId = driver.Id;

                await _rideService.Create(rideDto);

                return RedirectToAction("Rides", "Ride");
            }

            return View();

        }

        [HttpGet]
        [OutputCache(CacheProfile = "CarSharingCacheProfile")]
        public async Task<ActionResult> Details(int id)
        {
            var ride = await _rideService.GetWithDetail(id);
            var rideViewModel = _mapper.Map<RideDetailViewModel>(ride);
            return View(rideViewModel);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var ride = await _rideService.GetById(id);
            if (ride == null)
            {
                return HttpNotFound();

            }

            var driver = await _driverService.GetByUserName(User.Identity.Name);
            if (ride.DriverId != driver.Id)
            {
                return new HttpUnauthorizedResult();
            }
            var rideViewModel =  _mapper.Map<RideEditViewModel>(ride);
            return View(rideViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(RideEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var rideDto = _mapper.Map<RideDTO>(model);
                await _rideService.Update(rideDto);
            }

            return View();
        }


        [HttpGet]
        public async Task<ActionResult> Rides()
        {
            var rides = await _rideService.GetByDriverUsernameWithRequestsAndPassengers(User.Identity.Name);
            var ridesViewModel = _mapper.Map<IEnumerable<RideDetailViewModel>>(rides);
            return View(ridesViewModel);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var driver = await _driverService.GetByUserName(User.Identity.Name);
            if (driver==null)
            {
                return HttpNotFound("driver not found");
            }
            var ride = await _rideService.GetById(id);

            if (ride.DriverId!=driver.Id)
            {
                return new HttpUnauthorizedResult();
            }

            var ridesViewModel = _mapper.Map<RideEditViewModel>(ride);
            return View(ridesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(RideEditViewModel model)
        {
                await _rideService.DeleteById(model.Id);
            return RedirectToAction("Rides");
        }
    }
}