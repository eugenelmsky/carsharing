﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Helpers
{
    public class BirthDateAttribute : RangeAttribute
    {
        public BirthDateAttribute()
            : base(typeof(DateTime), DateTime.Now.AddYears(-80).ToShortDateString(), DateTime.Now.AddYears(-18).ToShortDateString()) { }
    }
}