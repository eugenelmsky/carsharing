﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Helpers
{
    public class RideTimeAttribute : RangeAttribute
    {
        public RideTimeAttribute()
            : base(typeof(DateTime), DateTime.Now.ToString(), DateTime.Now.AddYears(1).ToShortDateString())
        {
        }
    }
}