﻿using System;
using System.Linq;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.Web.Models;

namespace CarSharing.Web.Util
{
    public class WebLayerMapperProfile : Profile
    {
        public WebLayerMapperProfile()
        {
            CreateMap<PassengerDTO, PassengerProfileViewModel>()
                .ForMember(dest => dest.ChitChatAllowed, s => s.MapFrom(map => map.PassengerPreferences.ChitChatAllowed))
                .ForMember(dest => dest.MusicAllowed, s => s.MapFrom(map => map.PassengerPreferences.MusicAllowed))
                .ForMember(dest => dest.SmokingAllowed, s => s.MapFrom(map => map.PassengerPreferences.SmokingAllowed))
                .ForMember(dest => dest.PetAllowed, s => s.MapFrom(map => map.PassengerPreferences.PetAllowed))
                .ForMember(dest => dest.Years, s => s.MapFrom(src => new {timeSpan = DateTime.MinValue + (DateTime.Now.Date - src.BirthDate)}.timeSpan.Year-1))
                .ForMember(s => s.PassengerRating, r => r.MapFrom(src => src.PassengerRating))
                .ForMember(dest => dest.TimesRated, s => s.MapFrom(src => src.PassengerRating.Count()))
                .ForMember(dest => dest.TotalRating, s => s.MapFrom(src => src.PassengerRating.Any()==true ? ((float)src.PassengerRating.Sum(sr => sr.Rating) / src.PassengerRating.Count()) : 0 ))
                .ForAllMembers(tr => tr.AllowNull());
            CreateMap<PassengerProfileViewModel, PassengerDTO>();

            CreateMap<PassengerDTO, PassengerEditViewModel>()
                .ForMember(dest => dest.ChitChatAllowed, s => s.MapFrom(map => map.PassengerPreferences.ChitChatAllowed))
                .ForMember(dest => dest.MusicAllowed, s => s.MapFrom(map => map.PassengerPreferences.MusicAllowed))
                .ForMember(dest => dest.SmokingAllowed, s => s.MapFrom(map => map.PassengerPreferences.SmokingAllowed))
                .ForMember(dest => dest.PetAllowed, s => s.MapFrom(map => map.PassengerPreferences.PetAllowed))
                .ForAllMembers(s=>s.AllowNull());

            CreateMap<PassengerRegistrationViewModel, PassengerRegistrationDTO>().ReverseMap();
            CreateMap<DriverRegistrationViewModel, DriverRegistrationDTO>().ReverseMap();

            CreateMap<PassengerEditViewModel, PassengerEditDTO>()
                .ForPath(dest => dest.PassengerPreferences.ChitChatAllowed, s => s.MapFrom(map => map.ChitChatAllowed))
                .ForPath(dest => dest.PassengerPreferences.MusicAllowed, s => s.MapFrom(map => map.MusicAllowed))
                .ForPath(dest => dest.PassengerPreferences.SmokingAllowed, s => s.MapFrom(map => map.SmokingAllowed))
                .ForPath(dest => dest.PassengerPreferences.PetAllowed, s => s.MapFrom(map => map.PetAllowed))
                .ForAllMembers(s => s.AllowNull());

            CreateMap<DriverRatingDTO, DriverRatingViewModel>()
                .ForMember(dest => dest.Passenger, s => s.MapFrom(src => src.Passenger))
                .ReverseMap();

            CreateMap<DriverDTO, DriverProfileViewModel>()
                .ForMember(dest => dest.ChitChatAllowed, s => s.MapFrom(map => map.DriverPreference.ChitChatAllowed))
                .ForMember(dest => dest.MusicAllowed, s => s.MapFrom(map => map.DriverPreference.MusicAllowed))
                .ForMember(dest => dest.SmokingAllowed, s => s.MapFrom(map => map.DriverPreference.SmokingAllowed))
                .ForMember(dest => dest.PetAllowed, s => s.MapFrom(map => map.DriverPreference.PetAllowed))
                .ForMember(dest => dest.Years, s => s.MapFrom(src => new { timeSpan = DateTime.MinValue + (DateTime.Now.Date - src.BirthDate) }.timeSpan.Year - 1))
                .ForMember(s=>s.DriverRating,r=>r.MapFrom(src=>src.DriverRating))
                .ForMember(dest=>dest.TimesRated,s=>s.MapFrom(src => src.DriverRating.Count()))
                .ForMember(dest => dest.TotalRating, s => s.MapFrom(src => src.DriverRating.Any() == true ? ((float)src.DriverRating.Sum(sr => sr.Rating) / src.DriverRating.Count()) : 0))
                .ForAllMembers(tr => tr.AllowNull());
            CreateMap<DriverProfileViewModel, DriverDTO>();

            CreateMap<DriverEditViewModel, DriverEditDTO>()
                .ForPath(dest => dest.DriverPreference.ChitChatAllowed, s => s.MapFrom(map => map.ChitChatAllowed))
                .ForPath(dest => dest.DriverPreference.MusicAllowed, s => s.MapFrom(map => map.MusicAllowed))
                .ForPath(dest => dest.DriverPreference.SmokingAllowed, s => s.MapFrom(map => map.SmokingAllowed))
                .ForPath(dest => dest.DriverPreference.PetAllowed, s => s.MapFrom(map => map.PetAllowed))
                .ForAllMembers(s => s.AllowNull());

            CreateMap<DriverDTO, DriverEditViewModel>()
                .ForMember(dest => dest.ChitChatAllowed, s => s.MapFrom(map => map.DriverPreference.ChitChatAllowed))
                .ForMember(dest => dest.MusicAllowed, s => s.MapFrom(map => map.DriverPreference.MusicAllowed))
                .ForMember(dest => dest.SmokingAllowed, s => s.MapFrom(map => map.DriverPreference.SmokingAllowed))
                .ForMember(dest => dest.PetAllowed, s => s.MapFrom(map => map.DriverPreference.PetAllowed))
                .ForAllMembers(s => s.AllowNull());

            CreateMap<DriverDTO,DriverViewModel>().ForAllMembers(s=>s.AllowNull());


            CreateMap<RideViewModel, RideDTO>()
                .ForMember(s => s.Driver, d => d.MapFrom(s => s.Driver))
                .ForAllMembers(s => s.AllowNull());

            CreateMap<RideDetailViewModel,RideDTO>()
                .ForMember(s => s.Driver, d => d.MapFrom(s => s.Driver))
                .ForMember(s => s.Requests, d => d.MapFrom(s => s.Requests))
                .ForMember(s=>s.DriverId, dest => dest.MapFrom(s=>s.DriverId))
                .ForAllMembers(s=>s.AllowNull());
            



            CreateMap<RideDTO, RideViewModel>()
                .ForMember(s => s.Driver, d => d.MapFrom(s => s.Driver))
                .ForMember(s => s.AvailableSeats, r => r.AllowNull());

            CreateMap<RideCreateViewModel, RideDTO>().ForAllMembers(s=>s.AllowNull());

            CreateMap<RideEditViewModel,RideDTO>().ForAllMembers(s=>s.AllowNull());



            CreateMap<PassengerDTO, PassengerViewModel>().ForAllMembers(s => s.AllowNull());
            CreateMap<RequestDTO, RequestViewModel>()
                .ForMember(d => d.Passenger, s => s.MapFrom(r => r.Passenger));

            CreateMap<RequestDTO,RequestCreateViewModel>()
                .ReverseMap()
                .ForAllMembers(s=>s.AllowNull());

            CreateMap<RequestDTO, RequestWithRideViewModel>()
                .ForMember(s=>s.Ride,m=>m.MapFrom(r=>r.Ride))
                .ForAllMembers(s=>s.AllowNull());

            CreateMap<RatingPassengerCreateViewModel,PassengerRatingDTO>()
                .ForAllMembers(s=>s.AllowNull());

            CreateMap<RatingDriverCreateViewModel, DriverRatingDTO>()
                .ForAllMembers(s => s.AllowNull());




        }
    }
}