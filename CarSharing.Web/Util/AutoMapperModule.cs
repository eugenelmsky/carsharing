﻿using AutoMapper;
using CarSharing.BLL.Infastructure;
using Ninject;
using Ninject.Modules;

namespace CarSharing.Web.Util
{
    public class AutoMapperModule : NinjectModule
    {
        public override void Load()
        {

            var mapperConfiguration = CreateConfiguration();
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();

            // This teaches Ninject how to create automapper instances say if for instance
            // MyResolver has a constructor with a parameter that needs to be injected
            Bind<IMapper>().ToMethod(ctx =>
                new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        }

        private MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new WebLayerMapperProfile());
                cfg.AddProfile(new BuisnessLayerMapperProfile());
            });

            return config;
        }
    }
}