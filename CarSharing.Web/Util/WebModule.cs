﻿using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;
using CarSharing.BLL.Interfaces;
using CarSharing.BLL.Services;
using CarSharing.Web.Services;
using Ninject.Modules;

namespace CarSharing.Web.Util
{
    public class WebModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IFormsAuthenticationService>().To<FormsAuthenticationService>();
            Bind<IPassengerService>().To<PassengerService>();
            Bind<IDriverService>().To<DriverService>().WithConstructorArgument("baseProfileImage", ConfigurationManager.AppSettings.Get("baseProfileImage"));
            Bind<IRideService>().To<RideService>();
            Bind<IRequestService>().To<RequestService>();
            Bind<IRatingsService>().To<RatingsService>();
            Bind<IPasswordService>().To<PasswordService>().WithConstructorArgument("globalSalt", ConfigurationManager.AppSettings.Get("globalSalt"));
            Bind<IEmailService>().To<EmailService>()
                .WithConstructorArgument("smtpClient", new SmtpClient())
                .WithConstructorArgument("fromEmail", GetEmailAdressFromWebConfig());
            Bind<IFileService>().To<FileService>();
            Unbind<ModelValidatorProvider>();
        }

        public string GetEmailAdressFromWebConfig()
        {
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            return section.From;
        }
    }
}
