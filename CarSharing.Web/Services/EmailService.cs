﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CarSharing.Web.Services
{
    public class EmailService : IEmailService
    {
        private readonly SmtpClient _smtpClient;
        private readonly string _fromEmail;

        public EmailService(SmtpClient smtpClient, string fromEmail)
        {
            _smtpClient = smtpClient;
            _fromEmail = fromEmail;
        }

        public async Task SendPasswordRecoveryCodeAsync(string email, string confirmationCode, string verifyUrl)
        {   
            var messager =
                $"<br/><br/>{Resources.Resource.ResetPasswordLink} <br/><br/> <a href = '{verifyUrl}'>{Resources.Resource.ClickHere}</a> ";
            string subject = Resources.Resource.ResetPassword;

            using (var message = new MailMessage(_fromEmail, email, subject, messager))
            {
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                message.IsBodyHtml = true;
                await _smtpClient.SendMailAsync(message);
            }
        }
    }
}