﻿using System;
using System.Web;
using System.Web.Security;
using CarSharing.Web.Models;

namespace CarSharing.Web.Services
{
    /// <summary>
    /// The authenthication service
    /// </summary>
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        /// <summary>
        /// Signs the in the given user.
        /// </summary>
        /// <param name="userName">The name of the user.</param>
        /// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
        /// <param name="role">The name of role</param>
        public void SignIn(string userName, bool createPersistentCookie, Role role)
        {
            var authTicket = new FormsAuthenticationTicket(
                1, // version
                userName, // user name
                DateTime.Now, // created
                DateTime.Now.AddMinutes(30), // expires
                createPersistentCookie, // persistent?
                role.ToString() // user Role
            );

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
            {
                Path = FormsAuthentication.FormsCookiePath
            };
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        /// <summary>
        /// Signs the out the current user.
        /// </summary>
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}