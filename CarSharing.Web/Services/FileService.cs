﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;

namespace CarSharing.Web.Services
{
    public class FileService : IFileService
    {
        public string Upload(HttpPostedFileBase file, string username)
        {
            string virtualPathForImages = WebConfigurationManager.AppSettings["virtualPathForImages"];

            // Map to a physical path on the server
            var path = HostingEnvironment.MapPath(virtualPathForImages);

            if (file != null && file.ContentLength > 0)
            {
                var imageName = username + Path.GetExtension(file.FileName);
                var imagePath = path + username + Path.GetExtension(file.FileName);

                using (var stream = new FileStream(Path.GetFullPath(imagePath), FileMode.OpenOrCreate))
                {
                    // Convert our uploaded file to an image
                    Image origImage = Image.FromStream(file.InputStream);
                    // Create a new bitmap with the size of our thumbnail
                    Bitmap tempBitmap = new Bitmap(300, 300);

                    // Create a new image that contains are quality information
                    Graphics newImage = Graphics.FromImage(tempBitmap);
                    newImage.CompositingQuality = CompositingQuality.Default;
                    newImage.SmoothingMode = SmoothingMode.Default;
                    newImage.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    // Create a rectangle and draw the image
                    Rectangle imageRectangle = new Rectangle(0, 0, 300, 300);
                    newImage.DrawImage(origImage, imageRectangle);

                    // Save the final file
                    tempBitmap.Save(stream, origImage.RawFormat);

                    // Clean up the resources
                    newImage.Dispose();
                    tempBitmap.Dispose();
                    origImage.Dispose();
                }

                var result = virtualPathForImages + imageName;

                return result;
            }

            throw new FileNotFoundException();
        }
    }
}        