﻿using CarSharing.Web.Models;

namespace CarSharing.Web.Services
{
    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie, Role role);
        void SignOut();

    }
}