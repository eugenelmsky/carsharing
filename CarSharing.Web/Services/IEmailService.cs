﻿using System.Threading.Tasks;

namespace CarSharing.Web.Services
{
    public interface IEmailService
    {
        Task SendPasswordRecoveryCodeAsync(string email, string confirmationCode, string verifyUrl);
    }
}