﻿using System.Web;

namespace CarSharing.Web.Services
{
    public interface IFileService
    {
         string Upload(HttpPostedFileBase file, string username);
    }
}