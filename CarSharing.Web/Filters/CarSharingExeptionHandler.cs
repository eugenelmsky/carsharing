﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace CarSharing.Web.Filters
{
    public class CarSharingHandleErrorAttribute : FilterAttribute, IExceptionFilter
    {
        //TODO: logger
        public void OnException(ExceptionContext exceptionContext)
        {
            if (!exceptionContext.ExceptionHandled)
            {
                if (exceptionContext.Exception is NullReferenceException)
                {
                    exceptionContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            {"controller", "Error"},
                            {"action", "NotFound"}
                        });
                    exceptionContext.ExceptionHandled = true;
                }
                else
                {
                    exceptionContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            {"controller", "Error"},
                            {"action", "ServerError"}
                        });
                    exceptionContext.ExceptionHandled = true;
                }


            }
        }
    }
}