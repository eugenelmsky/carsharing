﻿using System.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CarSharing.BLL.Infastructure;
using CarSharing.Web.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;

namespace CarSharing.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            #region Ninject
            NinjectModule webModule = new WebModule();
            NinjectModule serviceModule = new ServiceModule(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            NinjectModule autoMapperModule = new AutoMapperModule();
            var kernel = new StandardKernel(webModule, serviceModule, autoMapperModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            #endregion
        }
    }
}
