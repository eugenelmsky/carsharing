﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class RideViewModel
    {
        public int Id { get; set; }
        public int DriverId { get; set; }

        [Display(Name = "StartTime", ResourceType = typeof(Resources.Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm}")]
        public DateTime StartTime { get; set; }

        [Display(Name = "EndTime", ResourceType = typeof(Resources.Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy hh:mm}")]
        public DateTime EndTime { get; set; }

        [Display(Name = "SourceCity", ResourceType = typeof(Resources.Resource))]
        public string SourceCity { get; set; }

        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Display(Name = "ContributionPerHead", ResourceType = typeof(Resources.Resource))]
        public string ContributionPerHead { get; set; }

        [Display(Name = "LuggageSize", ResourceType = typeof(Resources.Resource))]
        public string LuggageSize { get; set; }


        [Display(Name = "Seats", ResourceType = typeof(Resources.Resource))]
        public int Seats { get; set; }

        [Display(Name = "RideDescription", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }

        [Display(Name = "AvailableSeats", ResourceType = typeof(Resources.Resource))]

        public int? AvailableSeats { get; set; }

        public DriverProfileViewModel Driver { get; set; }
    }
}