﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class RideSearchViewModel
    {
        [Display(Name = "SourceCity", ResourceType = typeof(Resources.Resource))]
        public string SourceCity { get; set; }

        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Display(Name = "StartTime", ResourceType = typeof(Resources.Resource))]
        public DateTime? StartTime { get; set; }
    }
}