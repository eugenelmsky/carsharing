﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarSharing.Web.Models
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "EmailErrorMessage")]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resource))]
        public string Email { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AccountType { get; set; }
    }
}