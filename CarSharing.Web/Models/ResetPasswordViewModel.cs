﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarSharing.Web.Models
{
    public class ResetPasswordViewModel
    {
        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,30}$", ErrorMessage = "Use 6 or more characters with a mix of letters, numbers in lower and upper case")]
        [StringLength(20, ErrorMessage = "Must be between 6 and 20", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resource))]
        public string Password { get; set; }

        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,30}$", ErrorMessage = "Use 6 or more characters with a mix of letters, numbers in lower and upper case")]
        [StringLength(20, ErrorMessage = "Must be between 6 and 20", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.Resource))]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "PasswordConfirmErrorMessage")]

        public string ConfirmPassword { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Code { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Email { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AccountType { get; set; }
    }
}