﻿using System;
using System.ComponentModel.DataAnnotations;
using CarSharing.Web.Helpers;

namespace CarSharing.Web.Models
{
    public class RideCreateViewModel
    {
        [Required]
        [DataType(DataType.DateTime)]
        [RideTime(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "RideTimeErrorMessage")]
        [Display(Name = "StartTime", ResourceType = typeof(Resources.Resource))]
        public DateTime StartTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [RideTime(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "RideTimeErrorMessage")]
        [Display(Name = "EndTime", ResourceType = typeof(Resources.Resource))]
        public DateTime EndTime { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "SourceCity", ResourceType = typeof(Resources.Resource))]
        public string SourceCity { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Required]
        [Display(Name = "ContributionPerHead", ResourceType = typeof(Resources.Resource))]
        public string ContributionPerHead { get; set; }

        [Required]
        [Display(Name = "LuggageSize", ResourceType = typeof(Resources.Resource))]
        public string LuggageSize { get; set; }

        [Required]
        [Display(Name = "Seats", ResourceType = typeof(Resources.Resource))]
        public int Seats { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Must be between 6 and 50", MinimumLength = 4)]
        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }
    }
}