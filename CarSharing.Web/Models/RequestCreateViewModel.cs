﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarSharing.Web.Models
{
    public class RequestCreateViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int RideId { get; set; }
        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Display(Name = "Message", ResourceType = typeof(Resources.Resource))]
        public string Message { get; set; }
    }
}