﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarSharing.Web.Models
{
    public class RatingPassengerCreateViewModel
    {

        [HiddenInput(DisplayValue = false)]
        [Required]
        public int PassengerId { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "Please enter valid integer Number")]
        [Display(Name = "Rating", ResourceType = typeof(Resources.Resource))]
        public int Rating { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Must be between 2 and 50", MinimumLength = 2)]
        [Display(Name = "Comment", ResourceType = typeof(Resources.Resource))]
        public string Comment { get; set; }
    }
}