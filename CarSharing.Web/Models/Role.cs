﻿namespace CarSharing.Web.Models
{
    public enum Role
    {
        Passenger = 1,
        Driver = 2
    }
}