﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class PassengerProfileViewModel
    {
        public string ProfileImage { get; set; }

        [Display(Name = "Username", ResourceType = typeof(Resources.Resource))]
        public string Username { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Resources.Resource))]
        public string Email { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }

        [Display(Name = "ContactNumber", ResourceType = typeof(Resources.Resource))]
        public string ContactNumber { get; set; }

        [Display(Name = "CreatedOn", ResourceType = typeof(Resources.Resource))]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Years", ResourceType = typeof(Resources.Resource))]
        public int Years { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }

        [Display(Name = "SmokingAllowed", ResourceType = typeof(Resources.Resource))]
        public bool SmokingAllowed { get; set; }

        [Display(Name = "PetAllowed", ResourceType = typeof(Resources.Resource))]
        public bool PetAllowed { get; set; }

        [Display(Name = "MusicAllowed", ResourceType = typeof(Resources.Resource))]
        public bool MusicAllowed { get; set; }

        [Display(Name = "ChitChatAllowed", ResourceType = typeof(Resources.Resource))]
        public bool ChitChatAllowed { get; set; }

        public IEnumerable<PassengerRatingViewModel> PassengerRating { get; set; }

        [Display(Name = "TotalRating", ResourceType = typeof(Resources.Resource))]
        public string TotalRating { get; set; }
        [Display(Name = "TimesRated", ResourceType = typeof(Resources.Resource))]
        public int TimesRated { get; set; }
    }
}