﻿using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class UserLoginViewModel
    {
        [Required]
        [RegularExpression("^[0-9A-Za-z_]{5,15}$", ErrorMessage = "Use 6 or more characters, you can use letters, numbers and underscore")]
        [Display(Name = "Username", ResourceType = typeof(Resources.Resource))]
        public string Username { get; set; }

        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,30}$", ErrorMessage = "Use 6 or more characters with a mix of letters, numbers in lower and upper case")]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resource))]
        public string Password { get; set; }
    }
}