﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarSharing.Web.Models
{
    public class RideEditViewModel
    {
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int DriverId { get; set; }

        [Display(Name = "StartTime", ResourceType = typeof(Resources.Resource))]
        public DateTime StartTime { get; set; }

        [Display(Name = "EndTime", ResourceType = typeof(Resources.Resource))]
        public DateTime EndTime { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "SourceCity", ResourceType = typeof(Resources.Resource))]
        public string SourceCity { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Display(Name = "ContributionPerHead", ResourceType = typeof(Resources.Resource))]
        public string ContributionPerHead { get; set; }

        [Display(Name = "LuggageSize", ResourceType = typeof(Resources.Resource))]
        public string LuggageSize { get; set; }

        [Display(Name = "Seats", ResourceType = typeof(Resources.Resource))]
        public int Seats { get; set; }

        [Display(Name = "RideDescription", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }
    }
}