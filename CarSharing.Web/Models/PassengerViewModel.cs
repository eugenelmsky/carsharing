﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class PassengerViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Resources.Resource))]
        public string Email { get; set; }

        [Display(Name = "Username", ResourceType = typeof(Resources.Resource))]
        public string Username { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }

        [Display(Name = "ContactNumber", ResourceType = typeof(Resources.Resource))]
        public string ContactNumber { get; set; }

        public string ProfileImage { get; set; }

        [Display(Name = "CreatedOn", ResourceType = typeof(Resources.Resource))]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "BirthDate", ResourceType = typeof(Resources.Resource))]
        public DateTime BirthDate { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime PasswordResetTokenCreatedOn { get; set; }
    }
}