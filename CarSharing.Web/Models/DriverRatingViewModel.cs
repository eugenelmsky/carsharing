﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class DriverRatingViewModel
    {
        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int DriverId { get; set; }

        [Display(Name = "Rating", ResourceType = typeof(Resources.Resource))]
        public int Rating { get; set; }

        [Display(Name = "Comment", ResourceType = typeof(Resources.Resource))]
        public string Comment { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMMM yy}")]
        public DateTime CreatedOn { get; set; }
        public PassengerViewModel Passenger { get; set; }
    }
}