﻿namespace CarSharing.Web.Models
{
    public class RequestChangeStatusViewModel
    {
        public int RideId { get; set; }
        public bool RequestStatus { get; set; }
    }
}