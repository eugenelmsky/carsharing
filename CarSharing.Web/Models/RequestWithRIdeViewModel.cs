﻿using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class RequestWithRideViewModel
    {
        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int RideId { get; set; }

        [Display(Name = "DestinationCity", ResourceType = typeof(Resources.Resource))]
        public string DestinationCity { get; set; }

        [Display(Name = "RequestStatus", ResourceType = typeof(Resources.Resource))]
        public bool? RequestStatus { get; set; }
        public RideViewModel Ride { get; set; }
    }
}