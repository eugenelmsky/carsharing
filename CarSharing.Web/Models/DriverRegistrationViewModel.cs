﻿using System;
using System.ComponentModel.DataAnnotations;
using CarSharing.Web.Helpers;

namespace CarSharing.Web.Models
{
    public class DriverRegistrationViewModel
    {
        [Required]
        [RegularExpression(@"^[\p{L}\p{M}']{2,30}$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "FirstNameErrorMessage")]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}']{2,30}$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "LastNameErrorMessage")]
        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "EmailErrorMessage")]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resource))]
        public string Email { get; set; }

        [Required]
        [RegularExpression("^[a-z][a-z0-9_]{3,20}$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "UsernameErrorMessage")]
        [Display(Name = "Username", ResourceType = typeof(Resources.Resource))]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^([0-9]{6,15})$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "PhoneNumberErrorMessage")]
        [Display(Name = "ContactNumber", ResourceType = typeof(Resources.Resource))]
        public string ContactNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [BirthDate(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "BirthDateErrorMessage")]
        [Display(Name = "BirthDate", ResourceType = typeof(Resources.Resource))]
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "CarMake", ResourceType = typeof(Resources.Resource))]
        public string CarMake { get; set; }

        [Required]
        [Display(Name = "CarModel", ResourceType = typeof(Resources.Resource))]
        public string CarModel { get; set; }

        [Required]
        [Display(Name = "CarRegistrationNumber", ResourceType = typeof(Resources.Resource))]
        public string CarRegistrationNumber { get; set; }

        [Required]
        [Display(Name = "CarColor", ResourceType = typeof(Resources.Resource))]
        public string CarColor { get; set; }

        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,30}$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "PasswordErrorMessage")]
        [StringLength(20, ErrorMessage = "Must be between 6 and 20", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resource))]
        public string Password { get; set; }

        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,30}$", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "PasswordErrorMessage")]
        [StringLength(20, ErrorMessage = "Must be between 6 and 20", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.Resource))]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "PasswordConfirmErrorMessage")]
        public string ConfirmPassword { get; set; }
    }
}