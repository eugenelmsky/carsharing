﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSharing.Web.Models
{
    public class DriverViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Username", ResourceType = typeof(Resources.Resource))]
        public string Username { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }

        [Display(Name = "ContactNumber", ResourceType = typeof(Resources.Resource))]
        public string ContactNumber { get; set; }

        public string ProfileImage { get; set; }

        [Display(Name = "BirthDate", ResourceType = typeof(Resources.Resource))]
        public DateTime BirthDate { get; set; }

        [Display(Name = "CreatedOn", ResourceType = typeof(Resources.Resource))]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime PasswordResetTokenCreatedOn { get; set; }

        [Display(Name = "CarMake", ResourceType = typeof(Resources.Resource))]
        public string CarMake { get; set; }

        [Display(Name = "CarModel", ResourceType = typeof(Resources.Resource))]
        public string CarModel { get; set; }

        [Display(Name = "CarRegistrationNumber", ResourceType = typeof(Resources.Resource))]
        public string CarRegistrationNumber { get; set; }

        [Display(Name = "CarColor", ResourceType = typeof(Resources.Resource))]
        public string CarColor { get; set; }
    }
}