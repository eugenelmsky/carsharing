﻿using System;
using System.ComponentModel.DataAnnotations;
using CarSharing.Web.Helpers;

namespace CarSharing.Web.Models
{
    public class PassengerEditViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "EmailErrorMessage")]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resource))]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[\p{L}\p{M}'\-]{2,15}$", ErrorMessage = "Only letters")]
        [Display(Name = "LastName", ResourceType = typeof(Resources.Resource))]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [BirthDate(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "BirthDateErrorMessage")]
        [Display(Name = "BirthDate", ResourceType = typeof(Resources.Resource))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime BirthDate { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "ContactNumber", ResourceType = typeof(Resources.Resource))]
        public string ContactNumber { get; set; }

        [StringLength(50, ErrorMessage = "Must be between 6 and 50", MinimumLength = 6)]
        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }


        [Display(Name = "SmokingAllowed", ResourceType = typeof(Resources.Resource))]
        public bool SmokingAllowed { get; set; }

        [Display(Name = "PetAllowed", ResourceType = typeof(Resources.Resource))]
        public bool PetAllowed { get; set; }

        [Display(Name = "MusicAllowed", ResourceType = typeof(Resources.Resource))]
        public bool MusicAllowed { get; set; }

        [Display(Name = "ChitChatAllowed", ResourceType = typeof(Resources.Resource))]
        public bool ChitChatAllowed { get; set; }
    }
}