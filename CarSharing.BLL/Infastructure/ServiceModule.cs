﻿using System.Data;
using System.Data.SqlClient;
using CarSharing.DAL.Interfaces;
using CarSharing.DAL.Repositories;
using Ninject.Modules;

namespace CarSharing.BLL.Infastructure
{
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;
        public ServiceModule(string connection)
        {
            _connectionString = connection;
        }
        public override void Load()
        {
            Bind<IDbConnection>().To<SqlConnection>().WithConstructorArgument("connectionString", _connectionString);
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}