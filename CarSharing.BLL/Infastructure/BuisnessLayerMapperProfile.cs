﻿using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.DAL.Entities;

namespace CarSharing.BLL.Infastructure
{
    public class BuisnessLayerMapperProfile : Profile
    {
        public BuisnessLayerMapperProfile()
        {
            #region Passenger
            CreateMap<PassengerRegistrationDTO, PassengerDTO>()
                .ForMember(s => s.Email, dest => dest.MapFrom(src => src.EmailAdress))
                .ForAllMembers(s => s.Condition(rc => rc != null));
            CreateMap<PassengerDTO, Passenger>().ForMember(d => d.PassengerPreference, s => s.AllowNull())
                .ReverseMap();
            CreateMap<PassengerDTO, Passenger>()
                .ForMember(d => d.PassengerPreference, s => s.MapFrom(r => r.PassengerPreferences))
                .ForMember(d => d.PassengerRating, s => s.MapFrom(src => src.PassengerRating))
                .ReverseMap();
            CreateMap<PassengerEditDTO, Passenger>().ForAllMembers(s => s.Condition(rc => rc != null));
            CreateMap<PassengerEditDTO, PassengerPreferencesDTO>()
                .ForPath(d => d.ChitChatAllowed, s => s.MapFrom(r => r.PassengerPreferences.ChitChatAllowed))
                .ForPath(d => d.MusicAllowed, s => s.MapFrom(r => r.PassengerPreferences.MusicAllowed))
                .ForPath(d => d.PetAllowed, s => s.MapFrom(r => r.PassengerPreferences.PetAllowed))
                .ForPath(d => d.SmokingAllowed, s => s.MapFrom(r => r.PassengerPreferences.SmokingAllowed))
                .ForAllMembers(m => m.AllowNull());
            #endregion

            #region Driver
            CreateMap<DriverRegistrationDTO, DriverDTO>()
                .ForAllMembers(s => s.Condition(rc => rc != null));
            CreateMap<DriverDTO, Driver>().ForMember(d => d.DriverPreference, s => s.AllowNull())
                .ReverseMap();
            CreateMap<DriverDTO, Driver>()
                .ForMember(d => d.DriverPreference, s => s.MapFrom(r => r.DriverPreference))
                .ForMember(d=>d.DriverRating,s=>s.MapFrom(src=>src.DriverRating))
                .ReverseMap();

            CreateMap<DriverEditDTO, Driver>().ForAllMembers(s => s.Condition(rc => rc != null));
            CreateMap<DriverEditDTO, DriverPreferenceDTO>()
                .ForPath(d => d.ChitChatAllowed, s => s.MapFrom(r => r.DriverPreference.ChitChatAllowed))
                .ForPath(d => d.MusicAllowed, s => s.MapFrom(r => r.DriverPreference.MusicAllowed))
                .ForPath(d => d.PetAllowed, s => s.MapFrom(r => r.DriverPreference.PetAllowed))
                .ForPath(d => d.SmokingAllowed, s => s.MapFrom(r => r.DriverPreference.SmokingAllowed))
                .ForAllMembers(m => m.AllowNull());

            #endregion

            CreateMap<RideDTO, Ride>()
                .ForPath(d => d.Driver.DriverPreference, s => s.Ignore())
                .ForMember(s => s.Driver, p => p.MapFrom(r => r.Driver));

            CreateMap<Ride, RideDTO>()
                .ForAllMembers(s=>s.AllowNull());

            CreateMap<RequestDTO, Request>()
                .ForMember(d => d.Passenger, s => s.MapFrom(r => r.Passenger));

            CreateMap<DriverRatingDTO, DriverRating>()
                .ForMember(d => d.Passenger, s => s.MapFrom(r => r.Passenger))
                .ForAllMembers(dest=>dest.AllowNull());
        }
    }
}