﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    public class DriverRatingService:IDriverRatingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DriverRatingService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<IEnumerable<DriverRatingDTO>> GetAll()
        {
           var driverRating = await _unitOfWork.DriverRatingRepository.GetAll();
            var driverRatingDto = _mapper.Map<IEnumerable<DriverRating>, IEnumerable<DriverRatingDTO>>(driverRating);
            return driverRatingDto;
        }

        public async Task<DriverRatingDTO> GetById(int id)
        {
            var driverRating = await _unitOfWork.DriverRatingRepository.GetById(id);
            var driverRatingDto = _mapper.Map<DriverRatingDTO>(driverRating);
            return driverRatingDto;
        }

        public async Task Create(DriverRatingDTO item)
        {
            var driverRating = _mapper.Map<DriverRating>(item);
            await _unitOfWork.DriverRatingRepository.Create(driverRating);
            _unitOfWork.Commit();
        }

        public async Task Update(DriverRatingDTO item)
        {
            var driverRating = _mapper.Map<DriverRating>(item);
            await _unitOfWork.DriverRatingRepository.Create(driverRating);
            _unitOfWork.Commit();
        }

        public async Task DeleteById(int id)
        {
            var driverRating = await _unitOfWork.DriverRatingRepository.GetById(id);
            if (driverRating != null)
            {
               await _unitOfWork.DriverRatingRepository.DeleteById(id);
               _unitOfWork.Commit();
            }
        }

        public async Task<IEnumerable<DriverRatingDTO>> GetByDriverIdWithPassenger(int driverId)
        {
            var driverRating  = await _unitOfWork.DriverRatingRepository.GetByDriverIdWithPassenger(driverId);
            var driverRatingDto = _mapper.Map<IEnumerable<DriverRatingDTO>>(driverRating);
            return driverRatingDto;
        }
    }
}