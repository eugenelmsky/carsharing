﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// This class contains a set of functions for working with User rating
    /// </summary>
    public class RatingsService : IRatingsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RatingsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Get a passenger rating by passenger Id
        /// </summary>
        /// <param name="passengerId">Passenger Id</param>
        /// <returns>rating received by ID</returns>
        public async Task<IEnumerable<PassengerRatingDTO>> GetByPassengerId(int passengerId)
        {
            var rating = await _unitOfWork.PassengerRatingRepository.GetByPassengerId(passengerId);
            var passengerDto = _mapper.Map<IEnumerable<PassengerRatingDTO>>(rating);
            return passengerDto;
        }

        /// <summary>
        /// Get a driver rating by driver Id
        /// </summary>
        /// <param name="driverId">Driver Id</param>
        /// <returns>rating received by ID</returns>
        public async Task<IEnumerable<DriverRatingDTO>> GetByDriverId(int driverId)
        {
            var rating = await _unitOfWork.DriverRatingRepository.GetByDriverId(driverId);
            var driverRatingDto = _mapper.Map<IEnumerable<DriverRatingDTO>>(rating);
            return driverRatingDto;
        }

        /// <summary>
        /// The method presents the logic for creating a driver's appraisal
        /// </summary>
        /// <param name="driverRatingDto">model for a new assessment</param>
        public async Task RateTheDriver(DriverRatingDTO driverRatingDto)
        {
            // Did the passenger rate this driver?
            var ratingExist = await _unitOfWork.DriverRatingRepository.Find(s =>
                s.PassengerId == driverRatingDto.PassengerId && s.DriverId == driverRatingDto.DriverId);
            
            // If not, create rating
            if (!ratingExist.Any())
            {
                var passengerRequests =
                    await _unitOfWork.RequestRepository.GetAllWithRideByPassengerId(driverRatingDto.PassengerId);

                // Check if users participated in one trip?
                var result = passengerRequests.FirstOrDefault(s =>
                    s.Ride.DriverId == driverRatingDto.DriverId && s.RequestStatus == true);

                // if true, create rating
                if (result != null)
                {
                    var driverRating = _mapper.Map<DriverRating>(driverRatingDto);
                    await _unitOfWork.DriverRatingRepository.Create(driverRating);
                    _unitOfWork.Commit();
                }
            }

        }

        /// <summary>
        /// The method presents the logic for creating a driver's appraisal
        /// </summary>
        /// <param name="passengerRatingDto">model for a new assessment</param>
        public async Task RateThePassenger(PassengerRatingDTO passengerRatingDto)
        {
            // Did the driver rate this passenger?
            var ratingExist = await _unitOfWork.PassengerRatingRepository.Find(s =>
                s.PassengerId == passengerRatingDto.DriverId && s.PassengerId == passengerRatingDto.PassengerId);

            // If not, create rating
            if (!ratingExist.Any())
            {
                var passengerRequests =
                    await _unitOfWork.RequestRepository.GetAllWithRideByPassengerId(passengerRatingDto.PassengerId);

                // Check if users participated in one trip?
                var result = passengerRequests.FirstOrDefault(s =>
                    s.Ride.DriverId == passengerRatingDto.DriverId && s.RequestStatus == true);

                // if true, create rating
                if (result != null)
                {
                    var passengerRating = _mapper.Map<PassengerRating>(passengerRatingDto);
                    await _unitOfWork.PassengerRatingRepository.Create(passengerRating);
                    _unitOfWork.Commit();
                }
            }
        }
    }
}