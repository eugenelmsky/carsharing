﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CarSharing.BLL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// Contains all methods for performing basic hashing functions
    /// </summary>
    public class PasswordService:IPasswordService
    {
        private readonly string _globalSalt;

        public PasswordService(string globalSalt)
        {
            _globalSalt = globalSalt;
        }
        /// <summary>
        /// Returns a random dynamic salt
        /// </summary>
        /// <returns>Returns a string that represents the random dynamic salt</returns>
        public string GenerateDynamicSalt()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var max_length = 32;
                var salt = new byte[max_length];
                rng.GetNonZeroBytes(salt);

                return Convert.ToBase64String(salt);
            }
        }

        /// <summary>
        /// Returns a hashed representation of the supplied password for the specified 
        /// </summary>
        /// <returns>Returns a string that represents the dynamic salt for user</returns>
        public string HashPassword(string password, string dynamicSalt)
        {
            byte[] passwordInBytes = Encoding.UTF8.GetBytes(password);
            byte[] dynamicSaltInBytes = Encoding.UTF8.GetBytes(dynamicSalt);
            byte[] globalSaltInBytes = Encoding.UTF8.GetBytes(_globalSalt);

            byte[] passwordHashInBytes;

            using (var sha = new SHA1CryptoServiceProvider())
            {
                var hashOfPassword = sha.ComputeHash(passwordInBytes);

                var firstAddition = hashOfPassword.Concat(dynamicSaltInBytes).ToArray();
                var hashOfPasswordAndDs = sha.ComputeHash(firstAddition);

                var secondAddition = hashOfPasswordAndDs.Concat(globalSaltInBytes).ToArray();
                passwordHashInBytes = sha.ComputeHash(secondAddition);
            }

            var paswordHash = Convert.ToBase64String(passwordHashInBytes);


            return paswordHash;
        }

        public string HashPassword(string password, string dynamicSalt, string globalSalt)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns result of a password hash comparison
        /// </summary>
        /// <param name="hashedPassword">The hash value for a user's stored password</param>
        /// <param name="providedPassword">The password supplied for comparison</param>
        /// <param name="dynamicSalt">The dynamic salt for current user</param>
        /// <returns>bool value indicating the result of a password hash comparison</returns>
        public bool VerifyPassword(string hashedPassword, string providedPassword, string dynamicSalt)
        {
            var hashedProvidedPassword = HashPassword(providedPassword, dynamicSalt);
            return Equals(hashedPassword, hashedProvidedPassword);
        }

    
        public string GeneratePasswordResetToken(string email)
        {
            byte[] hashOfEmail = Encoding.UTF8.GetBytes(email);
            byte[] hashOfGuid = Guid.NewGuid().ToByteArray();
            var resultBytes = hashOfEmail.Concat(hashOfEmail).ToArray();
            using (var sha1 = SHA1.Create())
            {
                byte[] hashBytes = sha1.ComputeHash(resultBytes);

                return Convert.ToBase64String(hashBytes);
            }


        }

    }
}