﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Infastructure;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// This class contains a set of functions for working with Passengers
    /// </summary>
    public class PassengerService:IPassengerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPasswordService _passwordService;

        public PassengerService(IUnitOfWork unitOfWork, IMapper mapper, IPasswordService passwordService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _passwordService = passwordService;
        }

        /// <summary>
        /// Verify username and password
        /// </summary>
        /// <param name="username">Passenger username</param>
        /// <param name="password">Passenger password</param>
        /// <returns>login result</returns>
        public async Task<bool> Login(string username, string password)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetByUsername(username);
            if (passenger == null)
            {
                return false;
            }

            var isEqual = _passwordService.VerifyPassword(passenger.PasswordHash, password, passenger.PasswordSalt);

            return isEqual;
        }

        /// <summary>
        /// The method creates password reset token
        /// </summary>
        /// <param name="email">Passenger email adress</param>
        public async Task<string> CreatePasswordResetToken(string email)
        {
            //  user with this address exists?
            var passenger = await _unitOfWork.PassengerRepository.GetByEmail(email);

            if (passenger != null)
            {
                // Generate password reset token
                passenger.PasswordResetToken = _passwordService.GeneratePasswordResetToken(passenger.Email);

                // Set time stamp
                passenger.PasswordResetTokenCreatedOn = DateTime.Now;

                // save passwordResetToken in database
                await _unitOfWork.PassengerRepository.Update(passenger);
                _unitOfWork.Commit();

                return passenger.PasswordResetToken;
            }
            else
            {
                throw new ValidationException("User not found", "");
            }
        }

        /// <summary>
        /// The method verify password reset token
        /// </summary>
        /// <param name="passwordResetToken">Password reset token</param>
        /// <param name="email">Passenger email</param>
        public async Task<bool> VerifyPasswordResetToken(string passwordResetToken, string email)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetByEmail(email);
            var passengerDto = _mapper.Map<PassengerDTO>(passenger);

            var resetToken = passengerDto.PasswordResetToken;
            if (passengerDto.PasswordResetTokenCreatedOn != null)
            {
                DateTime resetTokenCreatedOn = passengerDto.PasswordResetTokenCreatedOn.Value;

                TimeSpan timeSpan = DateTime.Now - resetTokenCreatedOn;

                // resetToken expire?
                if (passwordResetToken == resetToken && timeSpan.Hours < 1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The  method resets the password for the current passenger
        /// </summary>
        /// <param name="email">Passenger email adress</param>
        /// <param name="password">Passenger new password</param>
        /// <returns>Password is reset or not</returns>
        public async Task<bool> ResetPassword(string email, string password)
        {
            try
            {
                var user = await _unitOfWork.PassengerRepository.GetByEmail(email);

                string passwordSalt = _passwordService.GenerateDynamicSalt();
                string passwordHash = _passwordService.HashPassword(password, passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                await _unitOfWork.PassengerRepository.Update(user);
                _unitOfWork.Commit();
                return true;
            }

            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<PassengerDTO> GetByEmaill(string email)
        {
            if (email == null)
            {
                //TODO:// LOG???? OR WHAT?
                //throw new ValidationException("Email cannot be null", "");
            }

            var passenger = await _unitOfWork.PassengerRepository.GetByEmail(email);
            if (passenger == null)
            {
                //throw new ValidationException("Passenger not found", "");
            }
            return _mapper.Map<PassengerDTO>(passenger);
        }

        /// <summary>
        /// Get passenger by Id
        /// </summary>
        /// <param name="id">Passenger Id</param>
        public async Task<PassengerDTO> GetById(int id)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetById(id);
            if (passenger == null)
            {
                throw new ValidationException("Passenger not found", "");
            }

            return _mapper.Map<PassengerDTO>(passenger);
        }

        /// <summary>
        /// Update profile image for current user
        /// </summary>
        /// <param name="imageUrl">Image Url</param>
        /// /// <param name="username">Passenger username</param>
        public async Task UpdateProfileImage(string imageUrl, string username)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetByUsername(username);
            if (passenger!=null)
            {
                passenger.ProfileImage = imageUrl;
                await _unitOfWork.PassengerRepository.Update(passenger);
                _unitOfWork.Commit();
            }
        }

        /// <summary>
        /// Creates a new passenger and save it in database
        /// </summary>
        /// <param name="model">New passenger</param>
        public async Task CreatePassenger(PassengerRegistrationDTO model)
        {
            var passengerInDatabase = await _unitOfWork.PassengerRepository.GetByUsername(model.Username);
            if (passengerInDatabase!=null)
            {
                throw new ValidationException("This username is already registered", "");
            }

            string passwordSalt = _passwordService.GenerateDynamicSalt();
            string passworHash = _passwordService.HashPassword(model.Password, passwordSalt);

            string baseProfileImage = "/ProfileImages/no-avatar.png";

            var currentUser = _mapper.Map<PassengerDTO>(model);
            currentUser.CreatedOn = DateTime.Now;
            currentUser.PasswordSalt = passwordSalt;
            currentUser.PasswordHash = passworHash;
            currentUser.ProfileImage = baseProfileImage;
            currentUser.PasswordResetTokenCreatedOn = null;
            

            var passenger = _mapper.Map<Passenger>(currentUser);

            await _unitOfWork.PassengerRepository.Create(passenger);
            _unitOfWork.Commit();

            var user = await _unitOfWork.PassengerRepository.GetByUsername(passenger.Username);
            var newPreferenceForProfile = new PassengerPreference()
            {
                Id = user.Id
            };
            await _unitOfWork.PassengerPreferenceRepository.Create(newPreferenceForProfile);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Gets all passengers from the database, and transfers them to the WebLayer
        /// </summary>
        /// <returns>List of passengers</returns>
        public async Task<IEnumerable<PassengerDTO>> GetAll()
        {
            var passengers = await _unitOfWork.PassengerRepository.GetAll();
            return _mapper.Map<IEnumerable<PassengerDTO>>(passengers);
        }

        /// <summary>
        /// Get passenger by username with his preferences
        /// </summary>
        /// <param name="username">Passenger username</param>
        /// <returns>Passenger received by username with his preferences</returns>
        public async Task<PassengerDTO> GetWithPreferencesByUsername(string username)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetWithPreference(username);

            if (passenger == null)
            {
                throw new ValidationException("Passenger not found", "");
            }

            var passengerRatings = await _unitOfWork.PassengerRatingRepository.GetByPassengerIdWithDriver(passenger.Id);

            passenger.PassengerRating = passengerRatings;

            var passengerDto = _mapper.Map<PassengerDTO>(passenger);

            return passengerDto;
        }

        /// <summary>
        /// Get passenger by username
        /// </summary>
        /// <param name="username">Passenger username</param>
        /// <returns>Passenger received by username</returns>
        public async Task<PassengerDTO> GetByUserName(string username)
        {
            if (username == null)
            {
                throw new ValidationException("Passenger not found", "");
            }

            var passenger = await _unitOfWork.PassengerRepository.GetByUsername(username);
            if (passenger == null)
            {
                throw new ValidationException("Passenger not found", "");
            }
            return _mapper.Map<PassengerDTO>(passenger);
        }

        /// <summary>
        /// Updates an existing passenger and save it in database
        /// </summary>
        /// <param name="passengerDto">updated passenger</param>
        public async Task Update(PassengerEditDTO passengerDto)
        {
            var passengerfromDb = await _unitOfWork.PassengerRepository.GetByUsername(passengerDto.Username);

            var passengerAfterMerge = _mapper.Map<PassengerEditDTO, Passenger>(passengerDto, passengerfromDb);
            await _unitOfWork.PassengerRepository.Update(passengerAfterMerge);

            var passengerPreferencesDto = _mapper.Map<PassengerEditDTO, PassengerPreferencesDTO>(passengerDto);
            passengerPreferencesDto.Id = passengerfromDb.Id;

            var preference = _mapper.Map<PassengerPreference>(passengerPreferencesDto);
            await _unitOfWork.PassengerPreferenceRepository.Update(preference);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Removes a passenger from the database by Id
        /// </summary>
        /// <param name="id">Passenger Id</param>
        public async Task DeleteById(int id)
        {
            await _unitOfWork.PassengerRepository.DeleteById(id);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Sets the standard profile image for current user
        /// </summary>
        /// <param name="username">Passenger username</param>
        public async Task DeleteProfileImage(string username)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetByUsername(username);
            if (passenger != null)
            {
                //TODO: set base profileImage from config
                string baseProfileImage = "/ProfileImages/no-avatar.png";
                passenger.ProfileImage = baseProfileImage;

                await _unitOfWork.PassengerRepository.Update(passenger);
                _unitOfWork.Commit();
            }
        }

        public void Dispose()
        {
           _unitOfWork.Dispose();
        }
    }
}