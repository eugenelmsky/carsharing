﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// This class contains a set of functions for working with Rides
    /// </summary>
    public class RideService:IRideService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RideService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all rides from the database, and transfers them to the WebLayer
        /// </summary>
        /// <returns>List of Rides</returns>
        public async Task<IEnumerable<RideDTO>> GetAll()
        {
            var rides= _mapper.Map<IEnumerable<RideDTO>>(await _unitOfWork.RideRepository.GetAll());
            if (rides!=null)
            {
                return rides;
            }
            else
            {
                return null;
                //TODO: log? or what?
                // throw new ValidationException("Rides not found", "");
            }
        }

        /// <summary>
        /// Get ride by Id
        /// </summary>
        /// <param name="id">Ride Id</param>
        /// <returns>Ride received by ID</returns>
        public async Task<RideDTO> GetById(int id)
        {
            var ride = await _unitOfWork.RideRepository.GetById(id);
            var rideDto = _mapper.Map<RideDTO>(ride);
            return rideDto;
        }

        /// <summary>
        /// Creates a new ride and save it in database
        /// </summary>
        /// <param name="rideDto">New ride</param>
        public async Task Create(RideDTO rideDto)
        {
            var ride = _mapper.Map<Ride>(rideDto);
            await _unitOfWork.RideRepository.Create(ride);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Updates an existing ride and save it in database
        /// </summary>
        /// <param name="rideDto"></param>
        public async Task Update(RideDTO rideDto)
        {
                var ride = _mapper.Map<Ride>(rideDto);
                await _unitOfWork.RideRepository.Update(ride);
                _unitOfWork.Commit();
        }

        /// <summary>
        /// Removes a ride from the database by Id
        /// </summary>
        /// <param name="id">Ride Id</param>
        public async Task DeleteById(int id)
        {
            await _unitOfWork.RideRepository.DeleteById(id);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Gets all rides from the database with their drivers, and transfers them to the WebLayer
        /// </summary>
        /// <returns>List of Rides</returns>
        public async Task<IEnumerable<RideDTO>> GetAllWithDrivers()
        {
            var ridesWithDrivers = await _unitOfWork.RideRepository.GetAllWithDrivers();
            var ridesWithDriversDto = _mapper.Map<IEnumerable<RideDTO>>(ridesWithDrivers);
            return ridesWithDriversDto;
        }

        /// <summary>
        /// Gets full information about the ride by driver username
        ///</summary>
        /// <param name="username">Driver username</param>
        /// <returns>Ride with all necessary data</returns>
        public async Task<IEnumerable<RideDTO>> GetByDriverUsernameWithRequestsAndPassengers(string username)
        {
            // Get driver by username
            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);
            var driverDto = _mapper.Map<DriverDTO>(driver);

            // Get all rides for this driver
            var rides = await _unitOfWork.RideRepository.GetByDriverId(driverDto.Id);
            var ridesDto = _mapper.Map<IEnumerable<RideDTO>>(rides);

            var passengers = await _unitOfWork.PassengerRepository.GetAllWithRatingsAndPreference();
            var passengersDto = _mapper.Map<IEnumerable<PassengerDTO>>(passengers);

            var requests = await _unitOfWork.RequestRepository.GetAll();
            var requestsDto = _mapper.Map<IEnumerable<RequestDTO>>(requests);


            foreach (var ride in ridesDto)
            {
                // check if there are requests for this trip?
                var requestForCurrentRide = requestsDto.Where(s => s.RideId == ride.Id);

                // if requests for this trip exist we add to them their passengers
                if (requestForCurrentRide.Any())
                {
                    foreach (var request in requestForCurrentRide)
                    {
                        request.Passenger = passengersDto.FirstOrDefault(s => s.Id == request.PassengerId);
                    }
                }

                // Сheck if there are accepted requests?
                var approvedRequests = requestForCurrentRide.Where(s => s.RequestStatus == true);

                // if accepted request exist, fill AvaliableSeats property
                if (approvedRequests.Any())
                {
                    ride.AvailableSeats = ride.Seats - approvedRequests.Count();
                }
                else
                {
                    ride.AvailableSeats = ride.Seats;
                }

                ride.Requests = requestForCurrentRide;
            }

            return ridesDto;
        }


        /// <summary>
        /// Gets full information about the ride by ride id
        ///</summary>
        /// <param name="id">Ride Id</param>
        /// <returns>Ride with all necessary data</returns>
        public async Task<RideDTO> GetWithDetail(int id)
        {
            var ride = await _unitOfWork.RideRepository.GetById(id);
            var rideDto = _mapper.Map<RideDTO>(ride);

            var driver = await _unitOfWork.DriverRepository.GetByIdWithRatingsAndPreference(ride.DriverId);
            var driverDto = _mapper.Map<DriverDTO>(driver);

            var passengers = await _unitOfWork.PassengerRepository.GetAllWithRatingsAndPreference();

            // check if there are requests for this trip?
            var requestsForCurrentRide = await _unitOfWork.RequestRepository.GetByRideId(id);

            // if requests for this trip exist we add to them their passengers
            foreach (var item in requestsForCurrentRide)
            {
                var pass = passengers.FirstOrDefault(s => s.Id == item.PassengerId);
                item.Passenger = pass;
            }

            var requestsDto = _mapper.Map<IEnumerable<RequestDTO>>(requestsForCurrentRide);

            rideDto.Driver = driverDto;

            // Сheck if there are accepted requests?
            var approvedRequests = requestsDto.Where(s => s.RequestStatus == true);


            // if accepted request exist, fill AvaliableSeats property
            if (approvedRequests.Any())
            {
                rideDto.Requests = approvedRequests;
                rideDto.AvailableSeats = rideDto.Seats - approvedRequests.Count();
            }
            else
            {
                rideDto.AvailableSeats = rideDto.Seats;
            }

            return rideDto;
        }

        /// <summary>
        ///  Gets all rides for current Driver
        /// </summary>
        /// <param name="username">Driver username</param>
        /// <returns></returns>
        public async Task<IEnumerable<RideDTO>> GetByDriverUsername(string username)
        {
            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);

            var rides = await _unitOfWork.RideRepository.GetByDriverId(driver.Id);
            var ridesDto = _mapper.Map<IEnumerable<RideDTO>>(rides);

            return ridesDto;
        }


        /// <summary>
        /// Searching for a trip on the given parameters
        /// </summary>
        /// <param name="rideSearchDto">ride search model</param>
        /// <returns>Found rides</returns>
        public async Task<IEnumerable<RideDTO>> Search(RideSearchDTO rideSearchDto)
        {
            var rides = await _unitOfWork.RideRepository.Search(rideSearchDto.SourceCity, rideSearchDto.DestinationCity, rideSearchDto.StartDate);
            var drivers = await _unitOfWork.DriverRepository.GetAllWithRatingsAndPreference();

            var driversDto = _mapper.Map<IEnumerable<DriverDTO>>(drivers);
            var ridesDto = _mapper.Map<IEnumerable<RideDTO>>(rides);

            var requests = await _unitOfWork.RequestRepository.GetAll();

            foreach (var ride in ridesDto)
            {
                // Get information about the driver for the current trip
                ride.Driver = driversDto.FirstOrDefault(s => s.Id == ride.DriverId);

                // Сheck if there are accepted requests?
                var requestsForCurrentRide = requests.Where(s => s.RideId == ride.Id && s.RequestStatus == true);

                // if accepted request exist, fill AvaliableSeats property
                if (requestsForCurrentRide.Any())
                {
                    ride.AvailableSeats = ride.Seats - requestsForCurrentRide.Count();
                }
                else
                {
                    ride.AvailableSeats = ride.Seats;
                }
            }

            return ridesDto;
        }

    }
}