﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// This class contains a set of functions for working with Requests
    /// </summary>
    public class RequestService:IRequestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RequestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets all requests from the database, and transfers them to the WebLayer
        /// </summary>
        /// <returns>List of requests</returns>
        public async Task<IEnumerable<RequestDTO>> GetAll()
        {
            var requests = await _unitOfWork.RequestRepository.GetAll();
            var requestsDto = _mapper.Map<IEnumerable<RequestDTO>>(requests);
            return requestsDto;
        }

        /// <summary>
        /// Get request by Id
        /// </summary>
        /// <param name="id">Request Id</param>
        /// <returns>Request received by ID</returns>
        public async Task<RequestDTO> GetById(int id)
        {
            var request = await _unitOfWork.RequestRepository.GetById(id);
            var requestDto = _mapper.Map<RequestDTO>(request);
            return requestDto;

        }

        /// <summary>
        /// Creates a new request and save it in database
        /// </summary>
        /// <param name="item">New request</param>
        public async Task Create(RequestDTO item)
        {
            var request = _mapper.Map<Request>(item);
            await _unitOfWork.RequestRepository.Create(request);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Updates an existing request and save it in database
        /// </summary>
        /// <param name="item"></param>
        public async Task Update(RequestDTO item)
        {
            var request = _mapper.Map<Request>(item);
            await _unitOfWork.RequestRepository.Update(request);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Removes a request from the database by Id
        /// </summary>
        /// <param name="id">Request Id</param>
        public async Task DeleteById(int id)
        {
            await _unitOfWork.RequestRepository.DeleteById(id);
            _unitOfWork.Commit();
        }


        /// <summary>
        /// Gets all requests from the database with their passengers info, and transfers them to the WebLayer
        /// </summary>
        /// <param name="id">Ride Id</param>
        /// <returns>List of requests</returns>
        public async Task<IEnumerable<RequestDTO>> GetByRideIdWithPassenger(int id)
        {
            var requestForRide = await _unitOfWork.RequestRepository.GetByRideIdWithPassenger(id);
            var requests = _mapper.Map<IEnumerable<RequestDTO>>(requestForRide);

            return requests;
        }

        /// <summary>
        /// Gets all requests from the database with their passengers info, and transfers them to the WebLayer
        /// </summary>
        /// <param name="username">Passenger username</param>
        /// <returns>List of requests</returns>
        public async Task<IEnumerable<RequestDTO>> GetAllWithRideByPassengerUsername(string username)
        {
            var passenger = await _unitOfWork.PassengerRepository.GetByUsername(username);
            var requestsWithRide = await _unitOfWork.RequestRepository.GetAllWithRideByPassengerId(passenger.Id);
            var drivers = await _unitOfWork.DriverRepository.GetAllWithRatingsAndPreference();

            foreach (var request in requestsWithRide)
            {
     
                request.Ride.Driver = drivers.FirstOrDefault(s => s.Id == request.Ride.DriverId);
            }

            return _mapper.Map<IEnumerable<RequestDTO>>(requestsWithRide);
        }
    }
}