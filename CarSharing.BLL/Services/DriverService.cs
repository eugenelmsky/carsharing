﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarSharing.BLL.DTO;
using CarSharing.BLL.Infastructure;
using CarSharing.BLL.Interfaces;
using CarSharing.DAL.Entities;
using CarSharing.DAL.Interfaces;

namespace CarSharing.BLL.Services
{
    /// <summary>
    /// This class contains a set of functions for working with Drivers
    /// </summary>
    public class DriverService:IDriverService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPasswordService _passwordService;
        private readonly string _baseProfileImage;

        public DriverService(IUnitOfWork unitOfWork, IMapper mapper, IPasswordService passwordService, string baseProfileImage)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _passwordService = passwordService;
            _baseProfileImage = baseProfileImage;
        }

        /// <summary>
        /// Creates a new driver and save it in database
        /// </summary>
        /// <param name="model">New driver</param>
        public async Task CreateDriver(DriverRegistrationDTO model)
        {
            var driverInDatabase = await _unitOfWork.DriverRepository.GetByUsername(model.Username);
            if (driverInDatabase != null)
            {
                throw new ValidationException("This username is already registered", "");
            }

            string passwordSalt = _passwordService.GenerateDynamicSalt();
            string passworHash = _passwordService.HashPassword(model.Password, passwordSalt);

            string baseProfileImage = "/ProfileImages/no-avatar.png";

            var currentUser = _mapper.Map<DriverDTO>(model);
            currentUser.CreatedOn = DateTime.Now;
            currentUser.PasswordSalt = passwordSalt;
            currentUser.PasswordHash = passworHash;
            currentUser.ProfileImage = baseProfileImage;
            currentUser.PasswordResetTokenCreatedOn = null;


            var driver = _mapper.Map<Driver>(currentUser);

            await _unitOfWork.DriverRepository.Create(driver);
            _unitOfWork.Commit();

            var user = await _unitOfWork.DriverRepository.GetByUsername(driver.Username);
            var newPreferenceForProfile = new DriverPreference()
            {
                Id = user.Id
            };
            await _unitOfWork.DriverPreferenceRepository.Create(newPreferenceForProfile);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Get Driver by Id
        /// </summary>
        /// <param name="id">Driver Id</param>
        public async Task<DriverDTO> GetById(int id)
        {
            var driver = await _unitOfWork.DriverRepository.GetById(id);
            if (driver == null)
            {
                throw new ValidationException("Driver not found", "");
            }

            return _mapper.Map<DriverDTO>(driver);
        }

        /// <summary>
        /// Gets all drivers from the database, and transfers them to the WebLayer
        /// </summary>
        /// <returns>List of drivers</returns>
        public async Task<IEnumerable<DriverDTO>> GetAll()
        {
            var drivers = await _unitOfWork.DriverRepository.GetAll();
            return _mapper.Map<IEnumerable<DriverDTO>>(drivers);
        }

        public async Task<DriverDTO> GetByEmaill(string email)
        {
            if (email == null)
            {
                //TODO: LOG? OR WHAT?
                //throw new ValidationException("Email cannot be null", "");
            }

            var driver = await _unitOfWork.DriverRepository.GetByEmail(email);
            if (driver == null)
            {
                //throw new ValidationException("Driver not found", "");
            }
            return _mapper.Map<DriverDTO>(driver);
        }

        /// <summary>
        /// Get driver by username with his preferences
        /// </summary>
        /// <param name="username">driver username</param>
        /// <returns>driver received by username with his preferences</returns>
        public async Task<DriverDTO> GetWithPreferencesByUsername(string username)
        {
            var driver = await _unitOfWork.DriverRepository.GetWithPreference(username);
            if (driver == null)
            {
                throw new ValidationException("Driver not found", "");
            }

            var driverRating = await _unitOfWork.DriverRatingRepository.GetByDriverIdWithPassenger(driver.Id);

            driver.DriverRating = driverRating;

            var driverDto = _mapper.Map<DriverDTO>(driver);

            return driverDto;
        }

        /// <summary>
        /// Get driver by username
        /// </summary>
        /// <param name="username">driver username</param>
        /// <returns>driver received by username</returns>
        public async Task<DriverDTO> GetByUserName(string username)
        {
            if (username == null)
            {
                throw new ValidationException("Driver not found", "");
            }

            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);
            if (driver == null)
            {
                throw new ValidationException("Passenger not found", "");
            }
            return _mapper.Map<DriverDTO>(driver);
        }

        /// <summary>
        /// Updates an existing driver and save it in database
        /// </summary>
        /// <param name="driverDto">updated driver</param>
        public async Task Update(DriverEditDTO driverDto)
        {
            var driverFromDb = await _unitOfWork.DriverRepository.GetByUsername(driverDto.Username);
            var driverAfterMerge = _mapper.Map<DriverEditDTO, Driver>(driverDto, driverFromDb);
            await _unitOfWork.DriverRepository.Update(driverAfterMerge);

            var driverPreferenceDto = _mapper.Map<DriverEditDTO, DriverPreferenceDTO>(driverDto);
            driverPreferenceDto.Id = driverFromDb.Id;

            var preference = _mapper.Map<DriverPreference>(driverPreferenceDto);
            await _unitOfWork.DriverPreferenceRepository.Update(preference);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Removes a driver from the database by Id
        /// </summary>
        /// <param name="id">driver Id</param>
        public async Task DeleteById(int id)
        {
            await _unitOfWork.DriverRepository.DeleteById(id);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// The method verify password reset token
        /// </summary>
        /// <param name="passwordResetToken">Password reset token</param>
        /// <param name="email">Driver email</param>
        public async Task<bool> VerifyPasswordResetToken(string passwordResetToken, string email)
        {
            var driver = await _unitOfWork.DriverRepository.GetByEmail(email);
            var driverDto = _mapper.Map<DriverDTO>(driver);

            var resetToken = driverDto.PasswordResetToken;
            DateTime? resetTokenCreatedOn = driverDto.PasswordResetTokenCreatedOn;

            if (resetTokenCreatedOn!=null)
            {
                TimeSpan timeSpan = DateTime.Now - resetTokenCreatedOn.Value;

                // resetToken expire?
                if (passwordResetToken == resetToken && timeSpan.Hours < 1)
                {
                    return true;
                }
            }
            

            return false;
        }

        /// <summary>
        /// Verify username and password
        /// </summary>
        /// <param name="username">Driver username</param>
        /// <param name="password">Driver password</param>
        /// <returns>login result</returns>
        public async Task<bool> Login(string username, string password)
        {
            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);

            if (driver==null)
            {
                return false;
            }

            var isEqual = _passwordService.VerifyPassword(driver.PasswordHash, password, driver.PasswordSalt);
            return isEqual;
        }

        /// <summary>
        /// The  method resets the password for the current driver
        /// </summary>
        /// <param name="email">driver email adress</param>
        /// <param name="password">new password</param>
        /// <returns>Password is reset or not</returns>
        public async Task<bool> ResetPassword(string email, string password)
        {
            try
            {
                var user = await _unitOfWork.DriverRepository.GetByEmail(email);

                string passwordSalt = _passwordService.GenerateDynamicSalt();
                string passwordHash = _passwordService.HashPassword(password, passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                await _unitOfWork.DriverRepository.Update(user);
                _unitOfWork.Commit();
                return true;
            }


            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// The method creates password reset token
        /// </summary>
        /// <param name="email">Driver email adress</param>
        public async Task<string> CreatePasswordResetToken(string email)
        {
            //  user with this address exists?
            var driver = await _unitOfWork.DriverRepository.GetByEmail(email);
            if (driver != null)
            {
                // Generate password reset token
                driver.PasswordResetToken = _passwordService.GeneratePasswordResetToken(driver.Email);

                // Set time stamp
                driver.PasswordResetTokenCreatedOn = DateTime.Now;

                // save passwordResetToken in database
                await _unitOfWork.DriverRepository.Update(driver);
                _unitOfWork.Commit();

                return driver.PasswordResetToken;
            }
            else
            {
                throw new ValidationException("Driver not found", "");
            }
        }

        /// <summary>
        /// Update profile image for current user
        /// </summary>
        /// <param name="imageUrl">Image Url</param>
        /// /// <param name="username">Driver username</param>
        public async Task UpdateProfileImage(string imageUrl, string username)
        {
            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);
            if (driver != null)
            {
                driver.ProfileImage = imageUrl;
                await _unitOfWork.DriverRepository.Update(driver);
                _unitOfWork.Commit();
            }
        }

        /// <summary>
        /// Sets the standard profile image for current user
        /// </summary>
        /// <param name="username">driver username</param>
        public async Task DeleteProfileImage(string username)
        {
            var driver = await _unitOfWork.DriverRepository.GetByUsername(username);
            if (driver != null)
            {
                // set base profileImage from web config
                string baseProfileImage = _baseProfileImage;
                driver.ProfileImage = baseProfileImage;

                await _unitOfWork.DriverRepository.Update(driver);
                _unitOfWork.Commit();
            } 
        }
    }
}