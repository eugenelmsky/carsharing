﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarSharing.BLL.Interfaces
{
    public interface IService<TEntity> where TEntity:class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task Create(TEntity item);
        Task Update(TEntity item);
        Task DeleteById(int id);
    }
}