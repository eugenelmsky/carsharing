﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;


namespace CarSharing.BLL.Interfaces
{
    public interface IRatingsService
    {
        Task<IEnumerable<PassengerRatingDTO>> GetByPassengerId(int passengerId);
        Task RateTheDriver(DriverRatingDTO driverRatingDto);
        Task RateThePassenger(PassengerRatingDTO passengerRatingDto);
        Task<IEnumerable<DriverRatingDTO>> GetByDriverId(int driverId);
    }
}