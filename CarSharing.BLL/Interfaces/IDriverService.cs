﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;

namespace CarSharing.BLL.Interfaces
{
    public interface IDriverService
    {
        Task CreateDriver(DriverRegistrationDTO passenger);
        Task<DriverDTO> GetById(int id);
        Task<IEnumerable<DriverDTO>> GetAll();
        Task<DriverDTO> GetByEmaill(string email);
        Task<DriverDTO> GetWithPreferencesByUsername(string username);
        Task<DriverDTO> GetByUserName(string username);
        Task Update(DriverEditDTO passenger);
        Task DeleteById(int id);
        Task<bool> VerifyPasswordResetToken(string passwordResetToken, string email);
        Task<bool> Login(string username, string password);
        Task<bool> ResetPassword(string email, string password);
        Task<string> CreatePasswordResetToken(string email);
        Task UpdateProfileImage(string imageUrl, string username);
        Task DeleteProfileImage(string username);
    }
}