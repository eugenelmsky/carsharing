﻿namespace CarSharing.BLL.Interfaces
{
    public interface IPasswordService
    {
        string GenerateDynamicSalt();
        string GeneratePasswordResetToken(string email);
        string HashPassword(string password, string dynamicSalt);
        bool VerifyPassword(string hashedPassword, string providedPassword, string dynamicSalt);
    }
}