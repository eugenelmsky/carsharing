﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;

namespace CarSharing.BLL.Interfaces
{
    public interface IPassengerService:IDisposable
    {
        Task CreatePassenger(PassengerRegistrationDTO passenger);
        Task<PassengerDTO> GetById(int id);
        Task<IEnumerable<PassengerDTO>> GetAll();
        Task<PassengerDTO> GetByEmaill(string email);
        Task<PassengerDTO> GetWithPreferencesByUsername(string username);
        Task<PassengerDTO> GetByUserName(string username);
        Task Update(PassengerEditDTO passenger);
        Task DeleteById(int id);
        Task<bool> VerifyPasswordResetToken(string passwordResetToken, string email);
        Task<bool> Login(string username, string password);
        Task<bool> ResetPassword(string email, string password);
        Task<string> CreatePasswordResetToken(string email);
        Task UpdateProfileImage(string imageUrl, string username);
        Task DeleteProfileImage(string username);
    }
}