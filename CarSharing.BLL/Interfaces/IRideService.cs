﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;

namespace CarSharing.BLL.Interfaces
{
    public interface IRideService:IService<RideDTO>
    {
        Task<IEnumerable<RideDTO>> GetAllWithDrivers();
        Task<IEnumerable<RideDTO>> GetByDriverUsernameWithRequestsAndPassengers(string username);
        Task<RideDTO> GetWithDetail(int id);
        Task<IEnumerable<RideDTO>> GetByDriverUsername(string username);
        Task<IEnumerable<RideDTO>> Search(RideSearchDTO rideSearchDto);
    }
}