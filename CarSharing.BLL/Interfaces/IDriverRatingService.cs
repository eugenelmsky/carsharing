﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;

namespace CarSharing.BLL.Interfaces
{
    public interface IDriverRatingService:IService<DriverRatingDTO>
    {
        Task<IEnumerable<DriverRatingDTO>> GetByDriverIdWithPassenger(int driverId);
    }
}