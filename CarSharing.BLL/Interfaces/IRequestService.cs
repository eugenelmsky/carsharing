﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarSharing.BLL.DTO;

namespace CarSharing.BLL.Interfaces
{
    public interface IRequestService:IService<RequestDTO>
    {
        Task<IEnumerable<RequestDTO>> GetByRideIdWithPassenger(int id);
        Task<IEnumerable<RequestDTO>> GetAllWithRideByPassengerUsername(string username);
    }
}