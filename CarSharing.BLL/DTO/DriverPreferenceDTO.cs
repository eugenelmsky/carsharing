﻿namespace CarSharing.BLL.DTO
{
    public class DriverPreferenceDTO
    {
        public int Id { get; set; }
        public bool SmokingAllowed { get; set; }
        public bool PetAllowed { get; set; }
        public bool MusicAllowed { get; set; }
        public bool ChitChatAllowed { get; set; }
    }
}