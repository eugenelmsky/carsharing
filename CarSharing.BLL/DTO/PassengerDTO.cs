﻿using System;
using System.Collections.Generic;

namespace CarSharing.BLL.DTO
{
    public class PassengerDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string ProfileImage { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime BirthDate { get; set; }
        public string Description { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime? PasswordResetTokenCreatedOn { get; set; }
        public PassengerPreferencesDTO PassengerPreferences { get; set; }
        public IEnumerable<PassengerRatingDTO> PassengerRating { get; set; }

    }
}