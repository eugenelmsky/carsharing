﻿using System;

namespace CarSharing.BLL.DTO
{
    public class RideSearchDTO
    {
        public string SourceCity { get; set; }
        public string DestinationCity { get; set; }
        public DateTime? StartDate { get; set; }
    }
}