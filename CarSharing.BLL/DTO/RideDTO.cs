﻿using System;
using System.Collections.Generic;

namespace CarSharing.BLL.DTO
{
    public class RideDTO
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string SourceCity { get; set; }
        public string DestinationCity { get; set; }
        public string ContributionPerHead { get; set; }
        public string LuggageSize { get; set; }
        public int Seats { get; set; }
        public string Description { get; set; }
        public int? AvailableSeats { get; set; }
        public DriverDTO Driver { get; set; }
        public IEnumerable<RequestDTO> Requests { get; set; }
    }
}