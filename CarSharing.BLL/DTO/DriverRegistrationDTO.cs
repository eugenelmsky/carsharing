﻿using System;

namespace CarSharing.BLL.DTO
{
    public class DriverRegistrationDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email{ get; set; }
        public string Username { get; set; }
        public string ContactNumber { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime BirthDate { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarRegistrationNumber { get; set; }
        public string CarColor { get; set; }
    }
}