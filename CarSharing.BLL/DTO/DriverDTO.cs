﻿using System;
using System.Collections.Generic;

namespace CarSharing.BLL.DTO
{
    public class DriverDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string ProfileImage { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordResetToken { get; set; }
        public DateTime? PasswordResetTokenCreatedOn { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarRegistrationNumber { get; set; }
        public string CarColor { get; set; }
        public DriverPreferenceDTO DriverPreference { get; set; }
        public IEnumerable<DriverRatingDTO> DriverRating { get; set; }
    }
}