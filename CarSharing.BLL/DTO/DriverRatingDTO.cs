﻿using System;

namespace CarSharing.BLL.DTO
{
    public class DriverRatingDTO
    {
        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int DriverId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }
        public DriverDTO Driver { get; set; }
        public PassengerDTO Passenger { get; set; }
    }
}