﻿using System;

namespace CarSharing.BLL.DTO
{
    public class PassengerEditDTO
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string Description { get; set; }
        public DateTime BirthDate { get; set; }
        public PassengerPreferencesDTO PassengerPreferences { get; set; }
    }
}