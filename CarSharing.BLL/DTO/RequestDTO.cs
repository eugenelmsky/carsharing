﻿namespace CarSharing.BLL.DTO
{
    public class RequestDTO
    {
        public int Id { get; set; }
        public int PassengerId { get; set; }
        public int RideId { get; set; }
        public string DestinationCity { get; set; }
        public bool? RequestStatus { get; set; }
        public string Message { get; set; }
        public PassengerDTO Passenger { get; set; }
        public RideDTO Ride { get; set; }
    }
}