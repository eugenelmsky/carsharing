﻿namespace CarSharing.BLL.DTO
{
    public class PassengerPreferencesDTO
    {
        public int  Id{ get; set; }
        public bool SmokingAllowed { get; set; }
        public bool PetAllowed { get; set; }
        public bool MusicAllowed { get; set; }
        public bool ChitChatAllowed { get; set; }
    }
}